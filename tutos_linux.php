<?php
/**
 * Template Name: Tutoriales GNU/Linux
 *
 * Plantilla Wordpress para mostrar tutoriales de GNU+Linux.
 * Mariano López Minnucci <mlopezviedma@gmail.com> 10/10/2015
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

	if (isset($_POST['submit'])) {
		$search = $_POST["search"];
		$description = $_POST["description"];
		$category = $_POST["category"];
		$quote = $_POST["quote"];
	} else {
		$search = "";
		$description = "";
		$category = "";
		$quote = "";
	}

	$gitUrl = "http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master";
	$indexUrl = $gitUrl . "/tutos.db";
	$templateUrl = $gitUrl . "/plantilla/plantilla_tuto.zip";
	$indexFile = "tutos.db";
	$ch = curl_init();
	$fp = fopen($indexFile, 'w');
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_URL,$indexUrl);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);
	$lines = file($indexFile);
	$line = "";


	function show_table_header() {
		echo "<table>";
		echo "<tr>";
		echo "<th>Título</th>";
		echo "<th>Descripción</th>";
		echo "<th>Categoría</th>";
		echo "</tr>";
	}

	function show_table_item($iname,$ititle,$idesc,$icat) {
		echo "<tr>";
		echo "<td><a href=\"http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/".$iname."/".$iname.".pdf\" target=\"_blank\">".$ititle."</a></td>";
		echo "<td>".$idesc."</td>";
		echo "<td>".$icat."</td>";
		echo "</tr>";
	}

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>

			<p><form method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>/tutos-linux/">
			<table style="width:500px">
			<tr><td>Buscar por título:</td>
			<td><input type="text" size="12" name="search"></td></tr>
			<tr><td>Buscar por descripción:</td>
			<td><input type="text" size="12" name="description"></td></tr>
			<tr><td>Buscar por categoría:</td>
			<td><select name="category">
				<option value="">Todas</option>
				<?php
					$out = "";
					foreach($lines as $line_num => $line) {
						$string = explode("|", $line);
						$cats = str_replace("\n", '', "$string[3]");
						$cats = explode(" ",$cats);
						foreach($cats as $cat) {
							if(!preg_match("@".$cat."@","$out")) {
								$out = $out."@".$cat."@";
								echo "<option value=\"".$cat."\">".$cat."</option>";
							}
						}
					}
				?>
			</select></td></tr>
			<tr><td>Buscar en todos los campos:</td>
			<td><textarea rows="2" cols="40" name="quote" wrap="physical"></textarea></td></tr>
			<tr><td><input type="submit" value="Enviar" name="submit"></td></tr>
			</table>
		</form></p>
		<table>
		<?php
			show_table_header();
			foreach($lines as $line_num => $line) {
				$thisLine = explode("|", $line);
				$thisFilename = "$thisLine[0]";
				$thisTitle = "$thisLine[1]";
				$thisDesc = "$thisLine[2]";
				$thisCat = "$thisLine[3]";
	
				if (preg_match("~".$search."~i", "$thisTitle")
					&& preg_match("~".$description."~i", "$thisDesc")
					&& preg_match("~".$category."~i", "$thisCat")
					&& preg_match("~"."$quote"."~i", "$thisTitle"."$thisDesc"."$thisCat")) {
						show_table_item("$thisFilename","$thisTitle","$thisDesc","$thisCat");
				}
			}
		?>
		</table>
		<p>Si desea escribir un tutorial, puede descargar la plantilla de tutoriales <a href="<?php echo $templateUrl ?>">aquí</a>.</p>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
