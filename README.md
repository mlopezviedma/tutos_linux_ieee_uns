# Tutoriales GNU+Linux
<table>
<tr>
<td><strong>Tutorial</strong></td>
<td width="60%"><strong>Descripción</strong></td>
<td><strong>Categorías</strong></td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/meta/meta.pdf" target="_blank">Acerca de estos tutoriales</a></td>
<td width="60%">En qué consisten estos tutoriales, cómo deben leerse y qué hacer para escribir nuevos.</td>
<td>meta</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/categorias/categorias.pdf" target="_blank">Acerca de las categorías de estos tutoriales</a></td>
<td width="60%">Qué significan las categorías que ya existen y cuáles son los criterios para agregar nuevas.</td>
<td>meta</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/archivos/archivos.pdf" target="_blank">Manejo de archivos en consola</a></td>
<td width="60%">Ver, copiar, mover y eliminar archivos y directorios en consola.</td>
<td>basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/bash/bash.pdf" target="_blank">Primer acercamiento a bash</a></td>
<td width="60%">La shell Bash. Sintaxis. Funciones internas.</td>
<td>basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/enlaces/enlaces.pdf" target="_blank">Enlaces</a></td>
<td width="60%">Enlaces físicos y enlaces simbólicos. Qué son y cómo se trabajan.</td>
<td>basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/fhs/fhs.pdf" target="_blank">La Jerarquía de Sistema de Archivos Estándar en GNU/Linux (FHS)</a></td>
<td width="60%">Conocer la ubicación estándar de directorios y archivos en un sistema GNU/Linux</td>
<td>admin basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/man/man.pdf" target="_blank">Páginas de manual de programas</a></td>
<td width="60%">Páginas de Manual del sistema: cómo buscarlas, cómo leerlas.</td>
<td>basico doc gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/permisos/permisos.pdf" target="_blank">Usuarios, grupos y permisos</a></td>
<td width="60%">Cómo trabajan los permisos de archivos para los distintos usuarios y grupos de un sistema.</td>
<td>admin basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/procesos/procesos.pdf" target="_blank">Manejo de procesos</a></td>
<td width="60%">Ver información de los procesos (programas en ejecución) del sistema. Matar procesos.</td>
<td>basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/su/su.pdf" target="_blank">Su y Sudo</a></td>
<td width="60%">Cómo acceder a la cuenta root desde otra cuenta. El programa sudo.</td>
<td>admin basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/uso_de_disco/uso_de_disco.pdf" target="_blank">Uso de memoria en disco y memoria RAM</a></td>
<td width="60%">Ver información del uso de memoria en disco y uso de memoria RAM.</td>
<td>basico gnu</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/usuario_prueba/usuario_prueba.pdf" target="_blank">Crear un usuario de prueba</a></td>
<td width="60%">Cómo crear y usar un usuario de prueba para probar nuevas cosas en el sistema sin riesgo de perder información o dañar el sistema.</td>
<td>basico</td>
</tr>
<tr>
<td><a href="https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/vim_basico/vim_basico.pdf" target="_blank">El editor de texto Vim</a></td>
<td width="60%">Editar archivos de texto en la consola utilizando Vim.</td>
<td>basico herramientas</td>
</tr>
</table>

## Acerca de los tutoriales GNU+Linux

Este proyecto, generado por la Rama Estudiantil del IEEE de la Universidad 
Nacional del Sur (Bahía Blanca, Argentina) intenta proveer una documentación 
concisa relacionada con GNU+Linux para que el usuario que encuentre dificultad 
en un tema determinado pueda acudir a ella y encontrar rápidamente una solución 
concreta y explicada a su problema.

Esta documentación está formada por pequeños tutoriales, donde cada uno de 
ellos aborda una cuestión específica. De esta manera no es necesario que el 
usuario deba encontrarse con una lectura extensa y necesitar filtrar 
demasiada información solamente para solucionar un problema pequeño.

Acceda a 
http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/all_in_one.pdf 
para ver **todos los tutoriales en un solo archivo PDF**.

## Tutoriales Pendientes

- Editores de texto: nano, emacs
- Gestor de paquetes APT (dpkg)
- Gestor de paquetes pacman
- Entornos de escritorio (KDE, GNOME, XFCE)
- Agendar tareas: cron
- Redirecciones en Bash
- Configuración de bash básico: .bash{rc,_profile,_logout,_aliases,_functions},
        export, PS{1,2,3,4}, PATH
- Manejo de flujo texto (depende de redirecciones): cat, head, tail, sort, tac,
        uniq; grep, less (more), wc, seq, tee, yes
- systemd
- find básico
- mount
- more on users (id, /etc/{passwd,group})
- Restricted shells
- Distros: explicar qué es una distro propiamente.
        Diferencias entre las principales.

## tutos_manager.sh

<pre>
Modo de uso: ./tutos_manager.sh [ -n TUTO1 [TUTO2 ...] | -m [TUTO1 ...] |
                        -g [TUTO1 ...] | -p TUTO1 [TUTO2 ...] | -Utcahu ]
Script para el manejo de los tutoriales GNU+Linux. Sin argumentos imprime
información acerca de los tutoriales existentes.
  -n, --new TUTO [ TUTO2 ... ]  Crea un tutorial nuevo por cada argumento dado
  -c, --clean  Elimina todos los archivos auxiliares generados por LaTeX.
  -m, --make [ TUTO ... ]  Compila los tutoriales dados como argumentos. Si no
               se dan argumentos, se compilan todos los tutoriales existentes.
  -g, --generate-db [ TUTO ... ]  Imprime la base de datos que resultaría de
               los tutoriales dados como argumentos. Si no se dan argumentos,
               genera la base de datos para todos los tutoriales existentes.
  -U, --update-readme  Actualiza README.md con los tutoriales actuales"
  -p, --print-info TUTO [ TUTO2 ... ]  Imprime la información de cada tutorial
               en formato 'tabular' de LaTeX.
  -t, --make-template  Comprime la plantilla de tutoriales.
  -a, all-in-one  Genera un solo archivo PDF a partir de los tutoriales
               disponibles.
  -h, --help   Imprime esta ayuda y sale
  -u, --usage  Imprime el modo de uso y sale
</pre>

## tutos_linux.php

Este archivo php está diseñado como plantilla de Wordpress para generar una página web
con un buscador y una lista de los tutoriales existentes debajo del contenido de la página.
Si su página web no está implementada con Wordpress, edite este archivo para adaptarlo a sus necesidades.
