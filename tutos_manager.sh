#!/bin/bash
#
# tutos_manager.sh: Script para el manejo de los tutoriales GNU+Linux.
# Mariano López Minnucci <mlopezviedma@gmail.com> 30/10/2015

TUTOS_DIR=tutos
TEMPLATE_DIR=plantilla
TEMPLATE_NAME=plantilla_tuto
TEMPLATE_FILE=plantilla
DB_FILE=tutos.db
README_FILE=README.md
DOC_FILE=DOC.md
LATEX_CMD="pdflatex -shell-escape -interaction=nonstopmode"
LATEX_ERR=
COMPRESS_CMD="zip -r"
COMPRESSED_FILE_EXTENSION=".zip"

RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
PURPLE='\e[1;35m'
CYAN='\e[1;36m'
WHITE='\e[1;37m'
RESET='\e[0m'

function print_usage () {
	echo "Modo de uso: $0 [ -n TUTO1 [TUTO2 ...] | -m [TUTO1 ...] |"
	echo "                  -g [TUTO1 ...] | -p TUTO1 [TUTO2 ...] | -Utcahu ]"
	}

function print_help () {
	print_usage
	echo "Script para el manejo de los tutoriales GNU+Linux. Sin argumentos imprime"
	echo "información acerca de los tutoriales existentes."
	echo "  -n, --new TUTO [ TUTO2 ... ]  Crea un tutorial nuevo por cada argumento dado"
	echo "  -c, --clean  Elimina todos los archivos auxiliares generados por LaTeX."
	echo "  -m, --make [ TUTO ... ]  Compila los tutoriales dados como argumentos. Si no"
	echo "               se dan argumentos, se compilan todos los tutoriales existentes."
	echo "  -g, --generate-db [ TUTO ... ]  Imprime la base de datos que resultaría de"
	echo "               los tutoriales dados como argumentos. Si no se dan argumentos,"
	echo "               genera la base de datos para todos los tutoriales existentes."
	echo "  -U, --update-readme  Actualiza README.md con los tutoriales actuales"
	echo "  -p, --print-info TUTO [ TUTO2 ... ]  Imprime la información de cada tutorial"
	echo "               en formato 'tabular' de LaTeX."
	echo "  -t, --make-template  Comprime la plantilla de tutoriales."
	echo "  -a, all-in-one  Genera un solo archivo PDF a partir de los tutoriales"
	echo "               disponibles."
	echo "  -h, --help   Imprime esta ayuda y sale"
	echo "  -u, --usage  Imprime el modo de uso y sale"
}

function message () { echo -e "${BLUE}:: ${WHITE}${@}${RESET}" >&2 ; }

function warning () { echo -e "${BLUE}::${YELLOW} Advertencia: ${WHITE}${@}${RESET}" >&2 ; }

function exiterr () {
	echo -e "${BLUE}::${RED} Error: ${@}${RESET}" >&2
	exit 1
}

function error_one_action () { exiterr "Solamente se admite una acción." ; }

function error_no_action () { exiterr "Opción no reconocida: $1" ; }

function ask_confirmation () {
	local OPTION
	echo -en "${BLUE}:: ${RESET}${@} ${WHITE}¿Está seguro? (s/n)${RESET} " >&2
	read -sn1 OPTION
	echo >&2
	if [ "$OPTION" == "s" ] || [ "$OPTION" == "S" ]; then
		return 0
	else
		message "${RED}Abortado por el usuario."
		exit 1
	fi
}

# catn N FICHERO imprime la línea N-ésima de FICHERO.
# catn N M FICHERO imprime desde la línea N-ésima hasta la M-ésima de FICHERO.
function catn ()
{
	local N M FILE
	if [ -n "$3" ]; then
		N=$1
		M=$2
		FILE=$3
	elif [ -n "$2" ]; then
		N=$1
		M=$1
		FILE=$2
	else return 1;
	fi  
	head -n $M $FILE | tail -n +$N 
}

function get_tutos () {
	local f tutos meta categorias
	for f in $(ls ${TUTOS_DIR}); do
		if [ -d "${TUTOS_DIR}/${f}" ]; then
			if [ "$f" == "meta" ]; then
				meta=yes
			elif [ "$f" == "categorias" ]; then
				categorias=yes
			else 
				tutos="${tutos} ${f}"
			fi
		fi
	done
	[ -n "$categorias" ] && tutos="categorias ${tutos}"
	[ -n "$meta" ] && tutos="meta ${tutos}"
	echo ${tutos}
}

function print_tutos_info () {
	if [ -n "$1" ]; then
		print_usage
		return 1
	fi
	local f title
	message "Tutoriales existentes:"
	for f in $(get_tutos); do
		echo -ne "${WHITE}${f}${RESET}"
		title="$(grep '\\def\\titulo{' ${TUTOS_DIR}/${f}/${f}.tex | cut -d{ -f2 | cut -d} -f1)"
		[ -n "$title" ] && echo -ne ": $title"
		echo
	done
}

function new_tutos () {
	local f
	if [ -z "$1" ]; then
		print_usage
		return 1
	fi
	[ -d "${TUTOS_DIR}" ] || mkdir -v ${TUTOS_DIR}
	for f in "$@"; do
		if [ -a "${TUTOS_DIR}/${f}" ]; then
			warning "Ya existe un tutorial con el nombre '${f}'."
		else
			message "Creando tutorial nuevo '${f}'..."
			mkdir -v ${TUTOS_DIR}/${f}
			cp -v ${TEMPLATE_DIR}/${TEMPLATE_NAME}/${TEMPLATE_FILE}/${TEMPLATE_FILE}.tex ${TUTOS_DIR}/${f}/${f}.tex
		fi
	done
	for f in $(ls ${TEMPLATE_DIR}/${TEMPLATE_NAME} | grep '\.pdf$'); do
		[ -f "${TUTOS_DIR}/${f}" ] || cp -v ${TEMPLATE_DIR}/${TEMPLATE_NAME}/${f} ${TUTOS_DIR}
	done
}

function clean () {
	local f
	if [ -n "$1" ]; then
		print_usage
		return 1
	fi
	message "Eliminando archivos auxiliares..."
	for f in $(get_tutos); do
		rm -rv ${TUTOS_DIR}/${f}/.minted 2>/dev/null
		rm -rv ${TUTOS_DIR}/${f}/_minted-${f} 2>/dev/null
		rm -v ${TUTOS_DIR}/${f}/*.{log,aux,out,pyg,tex.backup,synctex.gz} 2>/dev/null
	done
}

function print_info () {
	local file=$(mktemp)
	local name info
	for name in $@; do
		info="$(generate_db ${name})"
		echo "${name} & $(echo ${info} | cut -d'|' -f2) & $(echo ${info} | cut -d'|' -f3) \\\\" >> ${file}
	done
	cat ${file}
	rm -f ${file}
}

function make_tuto () {
	local FILE=${TUTOS_DIR}/${1}/${1}.tex
	if ! [ -f "$FILE" ]; then
		warning "No se encontró un tutorial con el nombre '${1}'."
		return 0
	fi
	message "Compilando tutorial '${1}'..."
	cd ${TUTOS_DIR}/${1}
	${LATEX_CMD} ${1}.tex && ${LATEX_CMD} ${1}.tex || \
		LATEX_ERR="${LATEX_ERR} ${1}"
	cd -
}

function make_tutos () {
	if [ -z "$1" ]; then
		ask_confirmation "Se compilarán todos los tutoriales."
		for f in $(get_tutos); do
			make_tuto $f
		done
	else
		for f in "$@"; do
			make_tuto $f
		done
	fi
	print_latex_err ${LATEX_ERR}
}

function print_latex_err () {
	local MSG
	if [ -z "${1}" ]; then
		return 0
	elif [ -z "${2}" ]; then
		MSG="El tutorial '${1}' no pudo compilarse."
	else
		MSG="Los siguientes tutoriales no pudieron compilarse: ${@}"
	fi
	exiterr "${MSG}"
}

function print_db_for_tuto () {
	local FILE TITLE BEGIN_ABSTRACT END_ABSTRACT DESCRIPTION CATEGORY
	FILE=${TUTOS_DIR}/${1}/${1}.tex
	if ! [ -f "$FILE" ]; then
		warning "No se encontró un tutorial con el nombre '${1}'."
		return 0
	fi
	TITLE="$(grep '\\def\\titulo{' $FILE | cut -d{ -f2 | cut -d} -f1)"
	BEGIN_ABSTRACT=$(grep -n '\\begin{abstract}' $FILE | cut -d: -f1)
	END_ABSTRACT=$(grep -n '\\end{abstract}' $FILE | cut -d: -f1)
	let BEGIN_ABSTRACT++
	let END_ABSTRACT--
	DESCRIPTION="$(catn $BEGIN_ABSTRACT $END_ABSTRACT $FILE | grep -v '^%' | sed 's/$/ /' | tr -d '\n' | sed 's/ $//')"
	CATEGORY="$(grep '\\def\\categoria{' $FILE | cut -d{ -f2 | cut -d} -f1)"
	echo "${1}|${TITLE}|${DESCRIPTION}|${CATEGORY}"
}

function generate_db () {
	if [ -z "$1" ]; then
		message "Generando base de datos de tutoriales completa en ${DB_FILE}..."
		for f in $(get_tutos); do
			print_db_for_tuto $f
		done > ${DB_FILE}
	else
		for f in "$@"; do
			print_db_for_tuto $f
		done
	fi
}

function update_readme ()
{
	local LINE FILE TITLE DESCRIPTION CATEGORY
	echo "# Tutoriales GNU+Linux" > $README_FILE
	echo "<table>" >> $README_FILE
	echo "<tr>" >> $README_FILE
	echo "<td><strong>Tutorial</strong></td>" >> $README_FILE
	echo "<td width=\"60%\"><strong>Descripción</strong></td>" >> $README_FILE
	echo "<td><strong>Categorías</strong></td>" >> $README_FILE
	echo "</tr>" >> $README_FILE
	cat $DB_FILE | while read LINE; do
		FILE="$(echo $LINE | cut -d'|' -f1)"
		TITLE="$(echo $LINE | cut -d'|' -f2)"
		DESCRIPTION="$(echo $LINE | cut -d'|' -f3)"
		CATEGORY="$(echo $LINE | cut -d'|' -f4)"
		echo "<tr>" >> $README_FILE
		echo "<td><a href=\"https://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/tutos/$FILE/$FILE.pdf\" target=\"_blank\">$TITLE</a></td>" >> $README_FILE
		echo "<td width=\"60%\">$DESCRIPTION</td>" >> $README_FILE
		echo "<td>$CATEGORY</td>" >> $README_FILE
		echo "</tr>" >> $README_FILE
	done
	echo "</table>" >> $README_FILE
	cat $DOC_FILE >> $README_FILE
	message "Archivo 'README.md' actualizado."
}

function make_template () {
	if [ -n "$1" ]; then
		print_usage
		return 1
	fi
	message "Comprimiendo plantilla de tutoriales..."
	cd ${TEMPLATE_DIR}
	[ -f ${TEMPLATE_NAME}${COMPRESSED_FILE_EXTENSION} ] && \
		rm -v ${TEMPLATE_NAME}${COMPRESSED_FILE_EXTENSION}
	${COMPRESS_CMD} ${TEMPLATE_NAME}${COMPRESSED_FILE_EXTENSION} ${TEMPLATE_NAME}
	cd -
}

function generate_all_in_one () {
	local old_wdir wdir cat_dir tex_name tutos tuto file
	local title description category cat
	local begin_def end_def title_def begin_abstract end_abstract end_content
	wdir=$(mktemp -d)
	tex_name=all_in_one
	cat_dir=categories
	old_wdir=$(pwd)
	tutos=$(get_tutos)
	cd $wdir
	cp ${old_wdir}/${TUTOS_DIR}/*.pdf .
	mkdir $tex_name $cat_dir
	cd $tex_name
	cp ${old_wdir}/${TEMPLATE_DIR}/${TEMPLATE_NAME}/${TEMPLATE_FILE}/${TEMPLATE_FILE}.tex original.tex
	begin_def=$(grep -n '\\def\\titulo{' original.tex | cut -d: -f1)
	end_def=$(grep -n '\\date{' original.tex | cut -d: -f1)
	let begin_def--
	let end_def++
	catn 1 $begin_def original.tex > phase.1.tex
	echo -e '\\def\\titulo{Tutoriales GNU+Linux}' >> phase.1.tex
	echo -e '\\def\\autor{Rama Estudiantil IEEE UNS}' >> phase.1.tex
	echo -e '\\def\\email{ieeeuns@uns.edu.ar}' >> phase.1.tex
	title_def=$(grep -n '\\title{' original.tex | cut -d: -f1)
	let title_def--
	catn $end_def $title_def original.tex >> phase.1.tex
	let title_def++
	let title_def++
	begin_abstract=$(grep -n '\\begin{abstract}' original.tex | cut -d: -f1)
	let begin_abstract--
	echo -e '\\title{\\Huge \\titulo}\\author{\\autor \\\\ \\ttfamily <\\email>}' >> phase.1.tex
	catn $title_def $begin_abstract original.tex >> phase.1.tex
	echo -e '\\renewcommand{\\thepart}{}\n' >> phase.1.tex
	echo -e '\\renewcommand{\\partname}{}\n\\maketitle' >> phase.1.tex
	for tuto in $tutos; do
		file=${old_wdir}/${TUTOS_DIR}/${tuto}/${tuto}.tex
		title="$(grep '\\def\\titulo{' $file | cut -d{ -f2 | cut -d} -f1)"
		begin_abstract=$(grep -n '\\begin{abstract}' $file | cut -d: -f1)
		end_abstract=$(grep -n '\\end{abstract}' $file | cut -d: -f1)
		let begin_abstract++
		let end_abstract--
		description="$(catn $begin_abstract $end_abstract $file | grep -v '^%' | sed 's/$/ /' | tr -d '\n' | sed 's/ $//')"
		category="$(grep '\\def\\categoria{' $file | cut -d{ -f2 | cut -d} -f1)"
		for cat in $category; do
			echo $tuto >> ../${cat_dir}/$cat
		done
		echo -e '\\newpage\n\\part{'"${title}"'}\label{'"${tuto}"'}\n' >> phase.3.tex
		echo -e '\\setcounter{section}{0}' >> phase.3.tex
		end_content=$(grep -n '{Nuestro canal IRC}' $file | cut -d: -f1)
		let begin_abstract--
		let end_content--
		catn $begin_abstract $end_content $file >> phase.3.tex
	done
	echo -e '\\end{document}' >> phase.3.tex
	echo -e '\\part*{Índice de tutoriales}' > phase.2.tex
	for tuto in $tutos; do
		file=${old_wdir}/${TUTOS_DIR}/${tuto}/${tuto}.tex
		title="$(grep '\\def\\titulo{' $file | cut -d{ -f2 | cut -d} -f1)"
		echo -e "$title"'\\dotfill\\pageref{'"${tuto}"'}\\par' >> phase.2.tex
	done
	echo -e '\\part*{Índice por categorías}' >> phase.2.tex
	for cat in $(ls ../${cat_dir}); do
		echo -e '\\subsection*{'"${cat}"'}' >> phase.2.tex
		for tuto in $(cat ../${cat_dir}/${cat}); do
			file=${old_wdir}/${TUTOS_DIR}/${tuto}/${tuto}.tex
			title="$(grep '\\def\\titulo{' $file | cut -d{ -f2 | cut -d} -f1)"
			echo -e "$title"'\\dotfill\\pageref{'"${tuto}"'}\\par' >> phase.2.tex
		done
	done
	echo -e '\\part*{Nuestro canal IRC}' >> phase.2.tex
	echo -e 'Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC' \
		'de \\emph{Freenode} \\textbf{\\#ramaieeeuns}.' >> phase.2.tex
	cat phase.{1,2,3}.tex > ${tex_name}.tex
	$LATEX_CMD ${tex_name}.tex && $LATEX_CMD ${tex_name}.tex || LATEX_ERR=yes
	cd $old_wdir
	[ -z "$LATEX_ERR" ] && mv -v ${wdir}/${tex_name}/${tex_name}.pdf . && \
		message "Archivo PDF con todos los tutoriales existentes generado en '${tex_name}.pdf'."
	rm -rf $wdir
	[ -n "$LATEX_ERR" ] && exiterr "No pudo generarse un archivo PDF con todos los tutoriales existentes."
}

action=
while [ -n "$1" ]; do # Parseamos las opciones de línea de comando
	case $1 in
		--) # Ya no leemos los argumentos como opciones
			shift
			break;;
		--*) # Opciones largas
			case ${1:2} in
				usage)         [ -z "$action" ] && action=print_usage || error_one_action;;
				help)          [ -z "$action" ] && action=print_help || error_one_action;;
				new)           [ -z "$action" ] && action=new_tutos || error_one_action;;
				clean)         [ -z "$action" ] && action=clean || error_one_action;;
				make)          [ -z "$action" ] && action=make_tutos || error_one_action;;
				generate-db)   [ -z "$action" ] && action=generate_db || error_one_action;;
				update-readme) [ -z "$action" ] && action=update_readme || error_one_action;;
				print-info)    [ -z "$action" ] && action=print_info || error_one_action;;
				make-template) [ -z "$action" ] && action=make_template || error_one_action;;
				all-in-one)    [ -z "$action" ] && action=generate_all_in_one || error_one_action;;
				*)             [ -z "$action" ] && error_no_action ${1} || error_one_action;;
			esac;;
		-*) # Opciones cortas
			for i in $(seq 2 ${#1}); do
				case ${1:i-1:1} in
					u) [ -z "$action" ] && action=print_usage || error_one_action;;
					h) [ -z "$action" ] && action=print_help || error_one_action;;
					n) [ -z "$action" ] && action=new_tutos || error_one_action;;
					c) [ -z "$action" ] && action=clean || error_one_action;;
					m) [ -z "$action" ] && action=make_tutos || error_one_action;;
					g) [ -z "$action" ] && action=generate_db || error_one_action;;
					U) [ -z "$action" ] && action=update_readme || error_one_action;;
					p) [ -z "$action" ] && action=print_info || error_one_action;;
					t) [ -z "$action" ] && action=make_template || error_one_action;;
					a) [ -z "$action" ] && action=generate_all_in_one || error_one_action;;
					*) [ -z "$action" ] && error_no_action -${1:i-1:1} || error_one_action;;
				esac
			done;;
		*) # No se han dado más opciones
			break;;
	esac
	shift
done

[ -z "$action" ] && action=print_tutos_info
$action $@

exit 0
