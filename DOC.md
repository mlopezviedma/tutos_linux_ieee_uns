
## Acerca de los tutoriales GNU+Linux

Este proyecto, generado por la Rama Estudiantil del IEEE de la Universidad 
Nacional del Sur (Bahía Blanca, Argentina) intenta proveer una documentación 
concisa relacionada con GNU+Linux para que el usuario que encuentre dificultad 
en un tema determinado pueda acudir a ella y encontrar rápidamente una solución 
concreta y explicada a su problema.

Esta documentación está formada por pequeños tutoriales, donde cada uno de 
ellos aborda una cuestión específica. De esta manera no es necesario que el 
usuario deba encontrarse con una lectura extensa y necesitar filtrar 
demasiada información solamente para solucionar un problema pequeño.

Acceda a 
http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/all_in_one.pdf 
para ver **todos los tutoriales en un solo archivo PDF**.

## Tutoriales Pendientes

- Editores de texto: nano, emacs
- Gestor de paquetes APT (dpkg)
- Gestor de paquetes pacman
- Entornos de escritorio (KDE, GNOME, XFCE)
- Agendar tareas: cron
- Redirecciones en Bash
- Configuración de bash básico: .bash{rc,_profile,_logout,_aliases,_functions},
        export, PS{1,2,3,4}, PATH
- Manejo de flujo texto (depende de redirecciones): cat, head, tail, sort, tac,
        uniq; grep, less (more), wc, seq, tee, yes
- systemd
- find básico
- mount
- more on users (id, /etc/{passwd,group})
- Restricted shells
- Distros: explicar qué es una distro propiamente.
        Diferencias entre las principales.

## tutos_manager.sh

<pre>
Modo de uso: ./tutos_manager.sh [ -n TUTO1 [TUTO2 ...] | -m [TUTO1 ...] |
                        -g [TUTO1 ...] | -p TUTO1 [TUTO2 ...] | -Utcahu ]
Script para el manejo de los tutoriales GNU+Linux. Sin argumentos imprime
información acerca de los tutoriales existentes.
  -n, --new TUTO [ TUTO2 ... ]  Crea un tutorial nuevo por cada argumento dado
  -c, --clean  Elimina todos los archivos auxiliares generados por LaTeX.
  -m, --make [ TUTO ... ]  Compila los tutoriales dados como argumentos. Si no
               se dan argumentos, se compilan todos los tutoriales existentes.
  -g, --generate-db [ TUTO ... ]  Imprime la base de datos que resultaría de
               los tutoriales dados como argumentos. Si no se dan argumentos,
               genera la base de datos para todos los tutoriales existentes.
  -U, --update-readme  Actualiza README.md con los tutoriales actuales"
  -p, --print-info TUTO [ TUTO2 ... ]  Imprime la información de cada tutorial
               en formato 'tabular' de LaTeX.
  -t, --make-template  Comprime la plantilla de tutoriales.
  -a, all-in-one  Genera un solo archivo PDF a partir de los tutoriales
               disponibles.
  -h, --help   Imprime esta ayuda y sale
  -u, --usage  Imprime el modo de uso y sale
</pre>

## tutos_linux.php

Este archivo php está diseñado como plantilla de Wordpress para generar una página web
con un buscador y una lista de los tutoriales existentes debajo del contenido de la página.
Si su página web no está implementada con Wordpress, edite este archivo para adaptarlo a sus necesidades.
