\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Páginas de manual de programas}
\def\categoria{basico doc gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{15 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Páginas de Manual del sistema: cómo buscarlas, cómo leerlas.
\end{abstract}

En GNU+Linux, todo lo que uno dispone en su sistema (sean programas, 
librerías, o cualquier otro tipo de utilidad) es instalado en forma de \emph{paquetes}. 
Es de común acuerdo que estos paquetes provean la documentación correspondiente en 
forma de \emph{páginas de manual}. Como resultado, uno tiene disponible la documentación 
de todos los paquetes instalados en el sistema para acceder a ellas cuando lo necesite.

\section{Buscar páginas de manual}

Para saber qué páginas de manual existen para un patrón de búsqueda dado, usamos el programa
\texttt{apropos}. Este programa busca las páginas de manual cuyo nombre o
descripción cumpla con el patrón de búsqueda dado.

\begin{bashcode}
apropos apropos      # Busca "apropos" en las páginas de manual
apropos "copy files" # Busca páginas de manual referidas a copiar archivos
\end{bashcode}

\section{Leer páginas de manual}

Las páginas de manual están separadas en ``secciones''. Cada página de manual pertenece a una 
entre nueve secciones dependiendo de qué es lo que se está documentando. Para ver una página de 
manual dada, ejecutamos 

\begin{bashcode}
man <sección> <nombre de la página de manual>
\end{bashcode}

Si no ingresamos ninguna sección, \texttt{man} mostrará la primera sección que exista en el sistema.
Para ver esto es suficiente ejecutar

\begin{bashcode}
man man
\end{bashcode}

y verificar que esto nos lleva a la sección 1 de la página de manual del programa \texttt{man}. 
Allí están listadas las nueve secciones, junto con algunas más que ya son obsoletas.

\bigskip
{\noindent\colorbox{bg}{
	\begin{tabular}{C{.1\textwidth} p{.8\textwidth}}
		\rm\bf Sección & \rm\bf Tipo de contenido \\
1 & Programas ejecutables y guiones del intérprete de órdenes \\
2 & Llamadas del sistema (funciones servidas por el núcleo) \\
3 & Llamadas de la biblioteca (funciones contenidas en las bibliotecas del sistema) \\
4 & Ficheros especiales (se encuentran generalmente en /dev) \\
5 & Formato de ficheros y convenios p.ej. /etc/passwd \\
6 & Juegos \\
7 & Paquetes de macros y convenios p.ej. man, groff. \\
8 & Órdenes de admistración del sistema (generalmente solo son para root) \\
9 &  Rutinas del núcleo [No es estándar] \\
	\end{tabular}}}
\bigskip

Navegue a través de la página con las flechas arriba/abajo o con las teclas RePág/AvPág, 
y presione \texttt{q} para salir. La opción \texttt{-a, -{}-{}all} de \texttt{man} hará que se 
muestren todas las secciones de la página de manual dada que existen en el sistema.

Si uno ya sabe que existen páginas de manual con un nombre dado, el programa \texttt{whatis}
mostrará la descripción de todas las secciones existentes. Por ejemplo,

\begin{bashcode}
whatis man
\end{bashcode}

mostrará las descripciones de \texttt{man(1)}, \texttt{man(1p)}\footnote{La letra \emph{p} 
indica que la página de manual sigue la norma POSIX (Interfaz de Sistema Operativo Portable 
UNIX) escrita por el IEEE.} y \texttt{man(7)}.

\reference{apropos & Buscar entre las páginas del manual y las descripciones & man-db \\
man & Interfaz de los manuales de referencia electrónicos & man-db \\
whatis & Imprime descripciones de páginas de manual & man-db \\}

\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
