\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Manejo de procesos}
\def\categoria{basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Ver información de los procesos (programas en ejecución) del sistema. Matar procesos.
\end{abstract}

Cuando un usuario ejecuta un programa se crea un nuevo \emph{proceso} en el sistema, el cual tendrá un número único
que lo identificará, llamado \emph{process ID} (PID).
El usuario que inició el proceso puede enviar \emph{señales} a este proceso (el usuario \texttt{root}
puede enviar señales a cualquier proceso). Al igual que para archivos, entonces, Linux asigna permisos a los procesos,
para enviar estas señales.

El programa \texttt{ps} muestra los procesos actuales. Con la opción \texttt{-e} muestra todos los procesos del sistema;
con la opción \texttt{-u, -{}-{}user} muestra todos los procesos ejecutados por un usuario específico.

\begin{bashcode}
ps -e     # Muestra todos los procesos actuales
ps -u foo # Muestra todos los procesos actuales ejecutados por foo
\end{bashcode}

Para buscar procesos específicos puede enviarse la salida al programa \texttt{grep}:

\begin{text}
% ps -e | grep X # Filtra las líneas que contienen "X"
10742 tty1     10:09:24 Xorg
\end{text}

El programa \texttt{kill} envía señales a los procesos cuyos PIDs son dados. Por defecto, \texttt{kill} envía la señal
\texttt{SIGTERM}, la cual pide al proceso que termine.

\begin{bashcode}
% ps -e | grep X
10742 tty1     10:09:24 Xorg
% kill 10742 # Envía la señal SIGTERM a esta instancia de Xorg
\end{bashcode}

El programa \texttt{killall} envía señales por nombre:

\begin{bashcode}
killall Xorg # Envía la señal SIGTERM al proceso cuyo nombre es Xorg
\end{bashcode}

El programa \texttt{pkill} permite buscar procesos según varios atributos.

\begin{bashcode}
pkill Xorg     # Envía la señal SIGTERM al proceso cuyo nombre es Xorg
ps -e | grep X
10742 tty1     10:09:24 Xorg
pkill -n 10742 # Envía la señal SIGTERM al proceso cuyo PID es 10742
\end{bashcode}

Para ver información de los procesos a tiempo real puede usarse \texttt{top}, o su versión más avanzada e interactiva
\texttt{htop}. Para matar \emph{ventanas} en un entorno gráfico de X, puede usarse \texttt{xkill}. Una vez que se ejecuta, 
el cursor del ratón cambiará, y se matará la ventana donde se haga un click.

\alert{Tenga cuidado con \texttt{xkill}. Si hace click en el escritorio, se matará el gestor de ventanas o el entorno de 
escritorio; y haciendo click en el panel se matará el panel.}

\reference{kill & Matar procesos & util-linux \\
killall & Matar procesos por nombre & psmisc \\
pkill & Buscar o enviar señales a procesos según ciertos atributos & procps-ng \\
ps & Ver información de procesos & procps-ng \\
top & Ver información detallada de procesos & procps-ng \\
htop & Visor de procesos interactivo & htop \\
xkill & Matar un cliente de X & xorg-xkill \\}
%\seealso{nombre & título & objetivo \\} % Referencia a tutoriales relacionados.
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
