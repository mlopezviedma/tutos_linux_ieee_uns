\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox,twoopt}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Primer acercamiento a bash} % Colocar el título del tutorial aquí
\def\categoria{basico gnu} % Colocar la/s categoría/s del tutorial aquí
\def\autor{Mariano López Minnucci} % Colocar el nombre del autor aquí
\def\email{mariano.lopezminnucci.ar@ieee.org} % Colocar el correo electrónico del autor aquí
\date{16 de octubre de 2015} % Colocar la fecha de la última edición para evitar que se sobreescriba

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}

% Comando para imprimir un cuadro informativo
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}

% Comando para imprimir un cuadro de alerta
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}

% Comando para imprimir una tabla. El primer argumento es opcional e indica el
% ancho de la primera columna (por defecto es el 20% del ancho de un párrafo).
% El segundo argumento también es opcional e indica el ancho de la segunda
% columna (por defecto es el 72.5% del ancho de un párrafo).
\newcommandtwoopt{\tabulartwo}[3][.2\textwidth][.725\textwidth]{
\bigskip\noindent\colorbox{bg}{
\begin{tabular}{C{#1} p{#2}}
#3
\end{tabular}}\bigskip}

% Comando para imprimir una referencia a programas usados y explicados.
\newcommand{\reference}[1]{
\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
#1
\end{tabular}}\bigskip}

% Comando para imprimir los tutoriales necesarios.
\newcommand{\depends}[1]{
\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}

% Comando para imprimir una referencia a tutoriales relacionados.
\newcommand{\seealso}[1]{
\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
La shell Bash. Sintaxis. Funciones internas.
\end{abstract}
%\depends{nombre & título & objetivo \\} % Tutoriales necesarios para comprender el contenido.

Para empezar, ¿Qué es Bash? Bash es el intérprete de comandos más común en las distribuciones GNU+Linux.
Un intérprete de comandos es un programa que provee al usuario una interfaz para acceder directamente a
las herramientas (programas) existentes en el sistema. Esta interfaz es un lenguaje interpretado, es decir,
un lenguaje al cual uno le da instrucciones línea por línea, y el intérprete los ejecuta también línea por línea.

\section{Sintaxis básica}

Normalmente, al abrir una terminal gráfica, consola virtual o terminal no gráfica (llamada comúnmente TTY)
el intérprete Bash se inicia automáticamente. El texto que se ve a la izquierda del cursor se llama \emph{prompt}.
Este indica que el intérprete está esperando que el usuario ingrese una orden, y también puede mostrar algún tipo
de información (normalmente el nombre de usuario, el nombre del host y el directorio de trabajo).

Como Bash es un lenguaje interpretado, podremos tomarnos todo el tiempo del mundo en escribir nuestra orden,
y ni bien presionemos la tecla \texttt{ENTER} Bash leerá la línea, verificará que la sintaxis es correcta y,
si lo es, ejecutará la orden. Aquí unos ejemplos:

\begin{textn}
mariano@mipc:~$ pwd
/home/mariano
mariano@mipc:~$ ls
curso_linux  Descargas  Imagenes
mariano@mipc:~$ ls curso_linux/ )
bash: error sintáctico cerca del elemento inesperado `)'
mariano@mipc:~$ ls curso_linux/
prueba.txt
mariano@mipc:~$ cd curso_linux/
mariano@mipc:~/curso_linux$ pwd
/home/mariano/curso_linux
\end{textn}

En el ejemplo el \emph{prompt} termina con el carácter \texttt{\$}. La orden ejecutada por el usuario es lo que está
a la derecha del \emph{prompt}. En la línea 6 vemos que Bash nos está diciendo que
no esperaba el carácter "\texttt{)}", y por lo tanto la orden de la línea 5 no fue ejecutada.

Dejando de lado por el momento los caracteres que Bash interpreta de una forma especial (como por ejemplo, los símbolos
\texttt{\$}, \texttt{\{} y \texttt{\}}), cada orden está compuesta por \emph{argumentos},
los cuales se obtienen separando la orden con \emph{espacios}. Lo primero que hace Bash al interpretar un comando es
verificar si el primer argumento es una \emph{función interna}.

\section{Funciones internas}

Las funciones internas de Bash proveen varias funcionalidades. Entre ellas está la función \texttt{help}, la cual
imprimirá en pantalla un texto de ayuda con todas las funciones internas. Podemos leer la ayuda detallada de una
función específica dándola como argumento, por ejemplo \texttt{help source} o \texttt{help for}.

La función \texttt{echo} muestra mensajes en pantalla, y es muy útil a la hora de escribir \emph{scripts}.

\begin{textn}
mariano@mipc:~$ echo "Hola, bash." # Imprime un mensaje en pantalla
Hola, bash.
mariano@mipc:~$ echo -n "Hola, bash." # La opción -n evita que se
# ingrese una nueva línea al final del mensaje
Hola, bash.mariano@mipc:~$ echo -en "Hola, bash.\nProbando echo.\n"
# Con la opción -e se admiten "caracteres de
# escape". En este caso, \n se traduce a un
# carácter de nueva línea.
Hola, bash.
Probando echo.
mariano@mipc:~$
\end{textn}

\info{La palabra \emph{script} proviene del inglés ``guión'' o ``libreto''. En lenguajes interpretados, un 
\emph{script} es un archivo de texto que contiene instrucciones escritas en el lenguaje en cuestión. Cuando este 
archivo es ejecutado, se abre una sesión del intérprete (\emph{subshell}) y este ejecuta las instrucciones como 
si el usuario las hubiera escrito manualmente.}

La función \texttt{alias}, como el nombre indica, hace un reemplazo del primer argumento ingresado en una orden.

\begin{textn}
mariano@mipc:~$ ls
curso_linux  Descargas  Imagenes
mariano@mipc:~$ ll
bash: ll: no se encontró la orden
mariano@mipc:~$ alias ll='ls -l' # Ahora "ll" significa "ls -l" siempre
# que sea escrito como primer argumento
mariano@mipc:~$ ll
drwxr-xr-x  4 mariano users 4,0K oct  9 09:51 curso_linux
drwxr-xr-x  3 mariano users 4,0K sep 15 13:14 Descargas
drwxr-xr-x  4 mariano users 4,0K sep 15 13:14 Imagenes
\end{textn}

\subsection{Manejo de directorios}

En el ejemplo de la sección anterior vimos las órdenes \texttt{pwd} y \texttt{cd}. Ambas son funciones internas de Bash.
\texttt{pwd} (de \emph{print working directory}) imprime el directorio de trabajo actual.
\texttt{cd} (de \emph{change directory}) cambia el directorio de trabajo. Por defecto (es decir, sin argumentos) 
nos lleva a nuestro directorio \texttt{HOME}. El argumento \texttt{-} (guión alto) nos lleva al último directorio en el que estuvimos.

\begin{bashcode}
cd foo/bar # Ir al directorio foo/bar.
cd         # Ir al directorio HOME.
cd -       # Volver al directorio foo/bar.
\end{bashcode}

\info{El carácter \texttt{\#} indica que todo lo que está a la derecha hasta el fin de línea es un comentario 
(es ignorado por el intérprete).}

Bash a su vez provee una \emph{pila} de directorios, muy útil cuando trabajamos en varios directorios.
La función \texttt{dirs} nos muestra la pila de directorios. Con \texttt{pushd} agregamos directorios a la pila
(o rotamos la pila) cambiando el directorio de trabajo. Con \texttt{popd} quitamos directorios de la pila.

\begin{textn}
~/curso_linux$ pushd tutoriales # agregamos 'tutoriales' a la pila
~/curso_linux/tutoriales ~/curso_linux
~/curso_linux/tutoriales$ pushd bash  # agregamos 'bash' a la pila
~/curso_linux/tutoriales ~/curso_linux ~/curso_linux/tutoriales/bash
~/curso_linux/tutoriales/bash$ pushd # rotamos la pila
~/curso_linux/tutoriales ~/curso_linux/tutoriales/bash ~/curso_linux
~/curso_linux/tutoriales$ popd # quitamos 'tutoriales' de la pila
~/curso_linux/tutoriales/bash ~/curso_linux
~/curso_linux/tutoriales/bash$ popd # quitamos 'bash' de la pila
~/curso_linux
~/curso_linux$ pwd # volvimos al punto de partida
/home/mariano/curso_linux
\end{textn}

En el ejemplo el \emph{prompt} nos muestra el directorio de trabajo actual.
Notar que es siempre el directorio que está encima en la pila.

\subsection{Sesión}

Al abrir una sesión de Bash, normalmente se importa el historial de órdenes a partir del archivo
\texttt{\~{}/.bash\_history} (el símbolo \texttt{\~} es una abreviatura de nuestro directorio \texttt{HOME}).
Uno puede acceder a las últimas órdenes ingresadas usando las flechas \emph{arriba} y \emph{abajo}.
Para trabajar con el historial de órdenes existe la función interna \texttt{history}.

\begin{textn}
mariano@mipc:~$ ls
curso_linux  Descargas  Imagenes
mariano@mipc:~$ alias ll='ls -l'
mariano@mipc:~$ ll
drwxr-xr-x  4 mariano users 4,0K oct  9 09:51 curso_linux
drwxr-xr-x  3 mariano users 4,0K sep 15 13:14 Descargas
drwxr-xr-x  4 mariano users 4,0K sep 15 13:14 Imagenes
mariano@mipc:~$ history # Muestra el historial de órdenes
1  16/10/15 16:43:31 ls
2  16/10/15 16:43:35 alias ll='ls -l'
3  16/10/15 16:45:37 ll
4  16/10/15 16:45:41 history
mariano@mipc:~$ history -c # Limpia el historial de órdenes
mariano@mipc:~$ history
1  16/10/15 16:46:05 history
\end{textn}

Para salir de la sesión de Bash, simplemente use la función \texttt{exit}.

%\subsection{Manejo de procesos}
%TODO: Ctrl+Z, bg, fg, jobs, kill

%\section{Autocompletado}
%TODO

%\section{Expansión}
%TODO

\reference{bash & GNU Bourne-Again SHell & bash }
\seealso{
archivos & Manejo de archivos en consola & Ver, copiar, mover y eliminar archivos y directorios en consola. \\
procesos & Manejo de procesos & Ver información de los procesos (programas en ejecución) del sistema. Matar procesos. \\
}
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
