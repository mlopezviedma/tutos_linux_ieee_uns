\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Uso de memoria en disco y memoria RAM}
\def\categoria{basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{24 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Ver información del uso de memoria en disco y uso de memoria RAM.
\end{abstract}

\section{Uso de disco}

Para ver el uso de disco de los distintos sistemas de archivos que existen en el sistema usamos 
\texttt{df}. Las cantidades normalmente se muestran en bytes; para obtener un formato más amigable, 
use la opción \texttt{-h, -{}-{}human-readable}.

\begin{bashcode}
df -h  # Muestra el uso de disco de los sistemas de archivos
df -ha # Con la opción -a, --all se muestran también los
       # pseudo-sistemas de archivos
\end{bashcode}

Si queremos saber el uso de disco de un archivo o conjunto de archivos (lo que comunmente llamamos 
\emph{tamaño en disco}) usamos \texttt{du} (disk usage), dando como argumentos los archivos de los que 
deseamos conocer su tamaño. Este programa también dispone de la opción \texttt{-h, -{}-{}human-readable}.

\begin{bashcode}
du -h foo.jpg  # Uso de disco del archivo foo.jpg
du -h *.pdf    # Uso de disco de todos los archivos en el directorio
               # actual cuyo nombre termina en .pdf
du -hc *.pdf   # La opción -c, --total hace que se muestre el total de
               # uso de disco de todos los argumentos
\end{bashcode}

Si damos un directorio como argumento, \texttt{du} mostrará el uso de disco de todos los archivos que contiene
(recursivamente) y por último mostrará el total. Con la opción \texttt{-s, -{}-{}summarize}, en cambio, \texttt{du}
mostrará solamente los totales de cada directorio que damos como argumento.

\begin{bashcode}
du -h .  # Uso de disco del directorio actual y todos los archivos que
         # contiene
du -sh . # Uso de disco total del directorio actual
\end{bashcode}

\section{Uso de RAM}

Para ver el uso de RAM del sistema usamos \texttt{free}. Al igual que \texttt{df} y \texttt{du}, Este programa 
dispone de la opción \texttt{-h, -{}-{}human-readable}. La opción \texttt{-t, -{}-{}total} hace que \texttt{free} muestre 
también el total de uso de RAM + Swap.

\info{En Linux la memoria de intercambio, también conocida como memoria Swap, queda a cargo de una partición especial 
del disco, para lo cual existe el sistema de archivos \emph{Linux Swap}. El sistema operativo usa la memoria Swap
para almacenar aquella información que no está siendo usada en el presente, de manera tal de disponer de una mayor 
cantidad de memoria RAM disponible (la cual puede leerse y escribirse a una velocidad mucho mayor) para los procesos
que están siendo usados ahora.}

\begin{text}
% free -h
          total    used    free  shared  buff/cache   available
Mem:       3,8G    1,5G    324M    543M        2,0G        1,7G
Swap:       15G    361M     15G
% free -ht
          total    used    free  shared  buff/cache   available
Mem:       3,8G    1,5G    321M    542M        2,0G        1,7G
Swap:       15G    361M     15G
Total:      19G    1,8G     15G
\end{text}

\reference{df & Ver el uso de disco en sistemas de archivos & coreutils \\
du & Ver el uso de disco en archivos & coreutils \\
free & Ver la memoria RAM usada y disponible en el sistema & procps-ng \\}
%\seealso{nombre & título & objetivo \\} % Referencia a tutoriales relacionados.
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
