\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Su y Sudo}
\def\categoria{admin basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{6 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}
\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}
\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}
\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Cómo acceder a la cuenta root desde otra cuenta. El programa sudo.
\end{abstract}

No hay razón para impedir al usuario \texttt{root} loguearse directamente en el sistema
y trabajar siempre con él. Sin embargo, hacer esto implica un riesgo demasiado grande para el
sistema porque \texttt{root} posee permisos suficientes para cambiar archivos esenciales del
sistema, y por lo tanto tiene el poder de dañarlo y hasta incluso destruirlo.

Un principio básico en administración de sistemas GNU+Linux, no importa si estamos administrando
un servidor, un entorno de escritorio o un sistema de prueba en una máquina virtual, es el siguiente:
\emph{Use la cuenta \texttt{root} solamente cuando sea absolutamente necesario}. Es por esto que
normalmente trabajamos con una cuenta de usuario regular, no pudiendo efectuar tareas de administración
directamente, logrando así un sistema seguro y robusto. De allí surge la idea de este tutorial.

\section{Su}

El programa \texttt{su} (switch user) sirve para loguearse como un usuario cualquiera desde la cuenta activa
de otro usuario. El argumento dado será el nombre del usuario destino (por omisión, \texttt{root}).
Adicionalmente podemos dar la opcion \texttt{-c, -{}-{}command}. Esta opción pide un argumento a su derecha,
el cual será una orden que el programa \texttt{su} intentará ejecutar una vez logueado, y a continuación
cerrará la sesión. Tener en cuenta que si el comando deseado posee a su vez argumentos, debemos encomillarlo
para evitar que el intérprete de comandos lea los espacios de forma especial.

Por supuesto, a no ser que uno parta de la cuenta de \texttt{root}, el programa \texttt{su} le pedirá la
contraseña del usuario en cuestión.

\begin{bashcode}
su                  # Cambia al usuario root
su foo              # Cambia al usuario foo
su -c "ls -l /root" # Ejecuta una orden como usuario root (lista el
                    # contenido del directorio HOME de root)
su -c "rm foo" bar  # Elimina el archivo foo como usuario bar
\end{bashcode}

\section{Sudo}

Prácticamente toda distribución pensada para usuarios de escritorio posee instalado el programa \texttt{sudo}\footnote{El 
nombre \emph{sudo} proviene de ``switch user and do'' (cambiar de usuario y luego hacer).}.
Este programa permite ejecutar ordenes como usuario \texttt{root} sin poseer su contraseña.

Desde el punto de vista de la seguridad, poseer una herramienta como esta es fundamentalmente incorrecto.
Sin embargo, resulta muy práctico cuando el sistema es manejado por un solo usuario.

\texttt{sudo} puede ser configurado de manera que pida o no contraseña\footnote{En contraste con el programa
\texttt{su}, la contraseña pedida por \texttt{sudo} será la del usuario ya logueado, y no la contraseña de
\texttt{root}. Esto puede parecer confuso al principio, pero hay que recordar que estos programas hacen cosas distintas.
\texttt{su} es legítimo: nos loguea como un determinado usuario. \texttt{sudo} es ilegítimo: efectua una tarea \emph{como
si fuera} otro usuario.}, y puede estar también limitado a un conjunto determinado de órdenes.

\begin{bashcode}
sudo apt-get update  # Actualiza la base de datos de paquetes en
                     # distribuciones Debian/Ubuntu/Mint
sudo apt-get upgrade # Actualiza el sistema en distribuciones
                     # Debian/Ubuntu/Mint
sudo pacman -Syu     # Actualiza la base de datos de paquetes y
                     # actualiza el sistema en distribuciones
                     # Archlinux/Parabola
sudo su              # Cambia al usuario root por medio de sudo
\end{bashcode}

\reference{
su & Cambiar de usuario & util-linux \\
sudo & Ejecutar programas como otro usuario & sudo \\
}
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
