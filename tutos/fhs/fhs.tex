% Plantilla de Tutoriales GNU+Linux - Rama Estudiantil IEEE UNS
% Mariano López Minnucci <mlopezviedma@gmail.com> 9/10/2015
\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{La Jerarquía de Sistema de Archivos Estándar en GNU/Linux (FHS)} % Colocar el título del tutorial aquí
\def\categoria{admin basico gnu} % Colocar la/s categoría/s del tutorial aquí
\def\autor{Marcelo López Minnucci} % Colocar el nombre del autor aquí
\def\email{coloconazo@gmail.com} % Colocar el correo electrónico del autor aquí
\date{17 de octubre de 2015} % Colocar la fecha de la última edición para evitar que se sobreescriba

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
#1
\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Conocer la ubicación estándar de directorios y archivos en un sistema GNU/Linux
\end{abstract}
%\depends{archivos & Manejo de archivos en consola & Ver, copiar, mover y eliminar archivos y directorios en consola \\} % Tutoriales necesarios para comprender el contenido.

% Contenido del tutorial
En un sistema GNU/Linux, la ubicación de los directorios y archivos y
los permisos correspondientes se ajustan, en mayor o menor medida, a un
estándar que se conoce como FHS (Filesystem Hierarchy Standard o Jerarquía
de Sistema de Archivos Estándar). Si bien esto depende de la distribución en
cuestión, en las principales distribuciones el sistema de archivos se ajusta
a este estándar; esto tiene la ventaja de que la ubicación de cualquier
archivo importante para el sistema es fácilmente predecible; por ejemplo, si
un paquete instala un archivo de configuración, casi con seguridad  lo
encontraremos en el directorio \texttt{/etc/}. Otra ventaja tiene que ver
con la seguridad de nuestro sistema, ya que al instalarse los archivos en
el directorio apropiado, se le asignarán también los permisos que correspondan.

\section{El directorio raíz}

A diferencia de otros sistemas operativos, en un sistema GNU/Linux cualquier
archivo o directorio se encuentra bajo el directorio raíz, o \texttt{/}. No
existe un directorio que esté ``por encima'' de \texttt{/}; de hecho, si nos
situamos en el directorio raíz (\texttt{cd /}) y ejecutamos \texttt{cd ..},
veremos que el directorio de trabajo sigue siendo \texttt{/}.

\section{La jerarquía primaria de directorios}

Si iniciamos sesión en una terminal y ejecutamos \texttt{cd /} y luego
\texttt{ls} veremos una lista de directorios: la \textbf{jerarquía primaria}
de directorios en GNU/Linux. En cada uno de estos directorios encontraremos
diferentes archivos y/o subdirectorios. A continuación describimos brevemente
qué tipo de archivos podemos esperar encontrar en cada uno de estos
subdirectorios.

\info{No todos los archivos y subdirectorios del sistema residen en disco; el
contenido de ciertos subdirectorios es una representación de lo que se conoce
como \emph{sistemas de archivos virtuales}, y residen en la memoria RAM, por
ejemplo \texttt{/proc}, \texttt{/tmp} y \texttt{/sys}. El directorio \texttt{/dev}
contiene archivos que representan los dispositivos del sistema, creados por
\texttt{udev}, y también residen en memoria.}

\subsection{\texttt{/bin}, \texttt{/sbin}}
El directorio \texttt{/bin} contiene \textbf{archivos ejecutables} (``programas'') esenciales
para el sistema, que pueden ser binarios (compilados) o scripts (archivos de texto interpretados,
usualmente scripts de bash).
El directorio \texttt{/sbin} contiene los ejecutables cuyo uso se reserva al administrador
del sistema o al usuario \textbf{root}, como \texttt{fsck}, \texttt{swapon},
\texttt{route}, etc.

A diferencia de \texttt{/usr/bin} y \texttt{/usr/sbin} (ver más abajo), el
contenido de \texttt{/bin} y \texttt{/sbin} se halla disponible en \emph{modo usuario
único}; este es el caso, por ejemplo, de las primeras etapas del arranque del
sistema. Todos los ejecutables que pudieran ser necesarios en \emph{modo usuario único}
deben residir en \texttt{/bin} o \texttt{/sbin}.

\subsection{\texttt{/boot}}
Contiene los archivos necesarios para el arranque del sistema, principalmente los
archivos asociados al gestor de arranque, el \emph{kernel} Linux y la \emph{initramfs}.

\subsection{\texttt{/dev}}
Contiene archivos especiales que representan los dispositivos existentes en el sistema;
estos archivos son creados y manejados por \texttt{udev}. Algunos archivos en \texttt{/dev}
no representan dispositivos reales sino que se conocen como \emph{dispositivos virtuales},
por ejemplo \texttt{/dev/null}, \texttt{/dev/zero} y \texttt{/dev/random}.

A modo de ejemplo, veamos cómo se hallan representados los dispositivos de almacenamiento.
Inicie sesión en una terminal y ejecute \texttt{ls /dev/sd*}. En mi sistema, el resultado
es:

\begin{bashcode}
ls /dev/sd*
/dev/sda  /dev/sda1  /dev/sda2  /dev/sda3
\end{bashcode}

\texttt{/dev/sda} representa un dispositivo de almacenamiento; \texttt{/dev/sda1},
\texttt{/dev/sda2} y \texttt{/dev/sda3} son las 3 particiones disponibles en el disco.

Ahora conecte un dispositivo de almacenamiento USB (un pendrive o disco extraíble) y ejecute
nuevamente \texttt{ls /dev/sd*}:

\begin{bashcode}
ls /dev/sd*
/dev/sda  /dev/sda1  /dev/sda2  /dev/sda3  /dev/sdb
/dev/sdb1
\end{bashcode}

Podemos ver que han sido creados dos archivos, uno para el dispositivo mismo y otro para su
única partición.

\subsection{\texttt{/etc}}
Este directorio contiene los \textbf{archivos de configuración} del sistema; tanto la
configuración esencial del sistema (usuarios, grupos, contraseñas, configuración del
arranque...) como los archivos de configuración de los diferentes paquetes instalados, se
almacenan en \texttt{/etc} en forma de archivos de texto plano, fácilmente editables mediante
un editor de texto. Así, por ejemplo, los usuarios del sistema se registran en \texttt{/etc/passwd},
los grupos en \texttt{/etc/group}, etcetera. El paquete \texttt{foo} instalará su archivo de
configuración como \texttt{/etc/foo.conf}. Para mantener \texttt{/etc} ordenado y limpio, si
un paquete instala más de un archivo de configuración, lo usual es crear un subdirectorio
\texttt{/etc/foo/}, e instalar los archivos allí.

\subsection{\texttt{/home}}
Contiene los directorios \emph{home} de los usuarios del sistema; en \texttt{/home} encontraremos
un subdirectorio por cada usuario, por ejemplo \texttt{/home/user}. En este directorio el usuario
almacenará sus propios archivos (descargas, archivos multimedia, documentos de todo tipo) y sus
archivos de configuración personalizados; estos son archivos cuyo nombre, por convención,
comienza con \texttt{.} (punto), por ejemplo \texttt{.bashrc}, el archivo de configuración de la
shell \texttt{bash}. (Los archivos cuyo nombre comienza con \texttt{.} se interpretan como
archivos ocultos, y el comando \texttt{ls} no los muestra, salvo que se lo invoque como
\texttt{ls -a}; los exploradores de archivos suelen también ocultar estos archivos por defecto.)

El directorio \emph{home} del usuario suele abreviarse con el signo \texttt{\~{}} (tilde); por
ejemplo, los comandos \texttt{cd /home/user} y \texttt{cd \~{}} son equivalentes.

\subsection{\texttt{/lib}}
Contiene las \emph{librerías} esenciales para el sistema;
si hacemos \texttt{ls /lib} veremos una larga lista de archivos llamados \texttt{lib*.so*},
\texttt{lib*.a}, etc. En \texttt{/lib/modules} se encuentran los módulos del kernel Linux. Pueden
existir otros directorios asociados a librerías de diferentes tipos, como \texttt{/lib64}, así
como diversos subdirectorios bajo \texttt{/lib}, como por ejemplo \texttt{/lib/firmware}.

\subsection{\texttt{/lost+found}}

Este directorio no forma parte del estándar, pero aparece en la raíz de los sistemas de archivos
ext. Cuando el sistema chequea la integridad de los archivos en el disco mediante \texttt{fsck},
los archivos dañados y recuperados son almacenados en este directorio, si bien no es seguro que
estén completos o en buenas condiciones.

\subsection{\texttt{/media}, \texttt{/mnt}}

Estos directorios se utilizan tradicionalmente como puntos de montaje; los dispositivos de
almacenamiento removibles usualmente se montan en un subdirectorio en \texttt{/media}, por ejemplo
\texttt{/media/cdrom} o \texttt{/media/usb}. El directorio \texttt{/mnt} suele usarse para
montar sistemas de archivos, por ejemplo para efectuar un \textbf{chroot}. Estas son sólo
convenciones, ya que es posible montar cualquier dispositivo en cualquier directorio del
sistema, pero mantener esta convención asegura un sistema ordenado y predecible, y por lo tanto
seguro.

\subsection{\texttt{/opt}}

El directorio \texttt{/opt} se utiliza para la instalación de paquetes que, por no ajustarse
cómodamente a la jerarquía estándar (por ejemplo, paquetes muy grandes contenidos en un solo
sirectorio, como pasa con algunos juegos, o paquetes con licencia no libre como MatLab, que al
no estar pensados para GNU/Linux no respetan esta jerarquía) se instalan completos en un subdirectorio
de \texttt{/opt}.

\subsection{\texttt{/proc}}

El directorio \texttt{/proc} es el punto de montaje del sistema de archivos virtual \emph{procfs},
cuyo contenido reside en la memoria RAM. Mediante \emph{procfs}, el kernel Linux expone información
acerca del estado actual del sistema: procesos, estado de diferentes componentes del hardware,
información del proceso de arranque, etc, etc. Podemos pensar \emph{procfs} como una interfaz que
nos permite visualizar el estado del sistema.

\texttt{/proc} contiene una serie de subdirectorios denominados \texttt{/proc/[pid]}, por ejemplo
\texttt{/proc/127}, donde se expone información acerca del proceso cuyo \emph{pid} es 127. Para
cada proceso en ejecución, existe el subdirectorio correspondiente en \texttt{/proc}.

\alert{No intente utilizar \texttt{/proc} para almacenar archivos de ningún tipo; de hecho esto no
es posible, pero aun si pudiéramos, sería peligrosísimo para la estabilidad del sistema.}

\subsection{\texttt{/root}}
Es el directorio \emph{home} del usuario \textbf{root}; por supuesto, sólo \textbf{root} tiene
acceso a su contenido. (No confundir con el directorio raíz \texttt{/}, también llamado ``root''
en inglés.)

\subsection{\texttt{/run}}
Contiene información sobre el estado del sistema desde el arranque; el contenido de \texttt{/run}
reside en memoria. A diferencia de \texttt{/proc}, \texttt{/run} es utilizado por los diferentes
procesos para almacenar información durante la ejecución.

\subsection{\texttt{/srv}}
Los diferentes \emph{servicios} disponibles en el sistema (por ejemplo, \textbf{http}, \textbf{ftp},
\textbf{www}\ldots) almacenan los archivos correspondientes en este directorio.

\subsection{\texttt{/tmp}}
Es el directorio utilizado para almacenar \emph{archivos temporarios}. Su contenido no reside en
disco sino en la memoria RAM; de hecho \texttt{/tmp} es el punto de montaje del sistema de archivos
virtual \emph{tmpfs}. Esto implica que si creamos un archivo en \texttt{/tmp}, el archivo no se
conservará luego de reiniciar el sistema. Una ventaja de utilizar la memoria RAM para almacenar
archivos temporarios es que ésta es mucho más rápida que el disco para procesos de
lectura/escritura. Tengamos en cuenta, si escribimos en \texttt{/tmp}, que su capacidad se halla
limitada por la cantidad de memoria disponible; si almacenamos archivos muy grandes en \texttt{/tmp}
corremos el riesgo de dejar al sistema sin memoria RAM libre.

\subsection{\texttt{/usr}, \texttt{/var}}
Constituyen las \textbf{jerarquías secundarias} de archivos, \emph{estáticos} y \emph{variables}
respectivamente; por su complejidad, se hallan explicadas en detalle en el siguiente capítulo.

\subsection{\texttt{/sys}}
Es el punto de montaje del sistema de archivos virtual \emph{sysfs}; al igual que \emph{procfs}, su
contenido reside en memoria, y contiene información expuesta por el kernel, principalmente sobre
dispositivos de hardware y drivers.

\section{Las jerarquías secundarias: \texttt{/usr} y \texttt{/var}}
%De acuerdo a la jerarquía estándar, los directorios \texttt{/bin}, \texttt{/sbin} y \texttt{/lib}
%contienen los ejecutables y librerías \emph{esenciales} para el sistema, de manera que se hallen
%disponibles desde las primeras etapas del arranque, cuando no es seguro que \texttt{/usr} y/o
%\texttt{/var} estén disponibles aún (podrían residir en otra partición de disco que \texttt{/} o
%incluso en una máquina remota).

\subsection{\texttt{/usr}}
El directorio \texttt{/usr} contiene una serie de subdirectorios similar, en cierto modo, a la
jerarquía primaria; si hacemos \texttt{ls /usr} veremos que contiene, entre otros, los
subdirectorios \texttt{/usr/bin}, \texttt{/usr/sbin} y \texttt{/usr/lib}. En general, los
paquetes instalados en el sistema almacenarán el grueso de su contenido en \texttt{/usr}.

La característica más saliente de \texttt{/usr} es que almacena archivos \textbf{estáticos}, es
decir, cuyo contenido no cambia por efecto de la ejecución de ningún proceso (excepto, claro está,
del gestor de paquetes, que seguramente instalará archivos allí). Podemos estar seguros de que,
salvo que como administradores modifiquemos expresamente el contenido de \texttt{/usr}, su contenido
permanecerá invariable. De hecho, según el estándar, \texttt{/usr} puede referirse a una partición
de disco local o remota, montada en modo \emph{sólo lectura}.

A continuación se describe el contenido de sus subdirectorios más importantes.

\paragraph{\texttt{/usr/bin}, \texttt{/usr/sbin}}
Contienen los ejecutables (programas) disponibles para todos los usuarios (\texttt{/usr/bin}) o
para el administrador del sistema únicamente (\texttt{/usr/sbin}). En estos directorios
encontraremos la gran mayoría de los programas propiamente dichos instalados en el sistema.

\paragraph{\texttt{/usr/include}}
Contiene las cabeceras necesarias para los programas escritos en C/C++.

\paragraph{\texttt{/usr/lib}}
Contiene las librerías utilizadas por los programas instalados en el sistema.

\paragraph{\texttt{/usr/local}}
Este subdirectorio constituye una \textbf{jerarquía de tercer orden}; contiene los mismos
subdirectorios que \texttt{/usr}. El propósito de \texttt{/usr/local} es almacenar archivos o
paquetes instalados localmente, es decir, no mediante el gestor de paquetes del sistema; por
ejemplo, los scripts o binarios creados o copiados manualmente deberían residir en
\texttt{/usr/local/bin} en lugar de en \texttt{/usr/bin}.

\paragraph{\texttt{/usr/share}}
Contiene todo tipo de archivos independientes de la arquitectura del sistema, y que pueden ser
compartidos (\emph{share}d) entre diversas aplicaciones, por ejemplo
íconos (\texttt{/usr/share/icons}),
fuentes (\texttt{/usr/share/fonts}),
documentación (\texttt{/usr/share/doc}), etc. Las páginas de manual del sistema se almacenan en
\texttt{/usr/share/man}.

\paragraph{\texttt{/usr/src}}
Cuando se desea almacenar código fuente, lo más usual es hacerlo en este directorio.

\subsection{\texttt{/var}}
A diferencia de \texttt{/usr}, \texttt{/var} contiene archivos y directorios cuyo contenido es
\emph{variable}; los archivos que vayan a ser modificados por un proceso durante su ejecución, por
ejemplo, bases de datos, archivos de registro (logs), etc., serán almacenados aquí. Nótese que
el contenido de \texttt{/var} no debe entenderse como ``archivos temporarios'', ya que reside en
disco y no en memoria, y por lo tanto se mantiene entre arranque y arranque del sistema (comparar
con \texttt{/tmp}, explicado más arriba).

Algunos subdirectorios de \texttt{/var}:

\paragraph{\texttt{/var/cache}}
Los programas que almacenan archivos en una caché pueden utilizar este directorio; por ejemplo, el
gestor de paquetes de Arch Linux, \texttt{pacman}, guarda los paquetes descargados en
\texttt{/var/cache/pacman/pkg}.

\paragraph{\texttt{/var/games}}
Como su nombre lo indica, es utilizado por juegos para almacenar archivos de contenido variable.

\paragraph{\texttt{/var/lib}}
En este directorio, las aplicaciones o servicios almacenan todo tipo de datos variables, por
ejemplo, bases de datos, información de estado, configuraciones, etc.

\paragraph{\texttt{/var/lock}}
Archivos utilizados para bloquear la ejecución de procesos (por ejemplo, para evitar que se inicien
múltiples instancias de un proceso, éste puede crear un archivo en \texttt{/var/lock}).

\paragraph{\texttt{/var/log}}
Archivos de registro (logs).

\paragraph{\texttt{/var/spool}}
Almacena archivos para ser procesados más tarde, por ejemplo, correo electrónico o archivos en
cola de impresión.

\section{Resumen}

Podemos ver que, en un sistema GNU/Linux, la ubicación de un determinado archivo es en buena medida
predecible según de qué tipo de archivo se trate; así, podemos asegurar que si la aplicación
\emph{foo}
instala ejecutables, archivos de configuración, imágenes, documentación y páginas de manual, casi
con seguridad podemos saber de antemano dónde buscar estos archivos:

\begin{itemize}
\item Ejecutables: en \texttt{/usr/bin} (por ejemplo \texttt{/usr/bin/foo})
\item Archivos de configuración: en \texttt{/etc} (\texttt{/etc/foo.conf} o \texttt{/etc/foo/*})
\item Imágenes: en \texttt{/usr/share} (\texttt{/usr/share/foo/images})
\item Documentación: en \texttt{/usr/share/doc/foo} o \texttt{/usr/share/foo/docs}
\item Páginas de manual: en \texttt{/usr/share/man/man1/foo.1.gz}
\end{itemize}

De hecho, los gestores de paquetes generalmente son capaces de mostrar la lista de archivos
instalados por cada paquete, de manera que, combinando esta funcionalidad con lo que ya conocemos
sobre la localización de los archivos, podemos hacer aún más trivial la tarea de ``rastrear''
archivos.

Supongamos que necesitamos saber qué páginas de manual ha instalado en el sistema el paquete
\emph{bash}. Mediante el gestor de paquetes, obtenemos la lista de archivos instalados por el
paquete; luego, sólo tenemos que filtrar los archivos que residen en \texttt{/usr/share/man}.
En Arch Linux, mediante el gestor de paquetes \texttt{pacman} y \texttt{grep}, este es el
resultado:

\begin{bashcode}
pacman -Qql bash | grep man
/usr/share/man/
/usr/share/man/man1/
/usr/share/man/man1/bash.1.gz     # Página de manual de bash
/usr/share/man/man1/bashbug.1.gz  # Página de manual de bashbug
\end{bashcode}
% \section{Una sección}
% \subsection{Una subsección}
% \subsection{Un párrafo con título.} Ejemplo de párrafo.
% \info{Esto es un cuadro informativo}
% \alert{Esto es un cuadro de alerta}
% \begin{bashcode}
% # Esto es un código de bash.
% \end{bashcode}
% \begin{text}
% Recuadro para mostrar textos planos.
% \end{text}
% \begin{textn}
% Recuadro para mostrar textos planos
% con números de líneas a la izquierda.
% \end{textn}
%\reference{nombre & descripción & paquete \\} % Referencia de todos los programas usados y explicados.
%\seealso{nombre & título & objetivo \\} % Referencia a tutoriales relacionados.
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
