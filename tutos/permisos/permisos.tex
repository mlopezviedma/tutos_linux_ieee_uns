\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Usuarios, grupos y permisos}
\def\categoria{admin basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{15 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}
\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}
\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}
\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Cómo trabajan los permisos de archivos para los distintos usuarios y grupos de un sistema.
\end{abstract}

GNU+Linux es un sistema multiusuario, donde cada usuario pertenece a uno o más grupos. Todo sistema de archivos en
GNU+Linux soporta entonces \emph{permisos} para asignar a cada archivo, los cuales controlan qué usuarios pueden efectuar
determinadas operaciones sobre cada uno de ellos. Este tutorial intenta explicar brevemente estos conceptos.

\section{Usuarios y grupos}

Para conocer nuestro nombre de usuario, podemos ejecutar:

\begin{bashcode}
whoami # Imprime el nombre del usuario que ejecutó la orden
\end{bashcode}

El programa \texttt{passwd} sirve para cambiar la contraseña de un usuario:

\begin{bashcode}
passwd     # Cambia la contraseña del usuario que ejecutó la orden
passwd foo # Cambia la contraseña del usuario foo (debe ser ejecutado
           # por el usuario root)
\end{bashcode}

En GNU+Linux, todo usuario pertenece a un grupo principal (llamado grupo primario), y puede pertenecer también a más
grupos llamados suplementarios. El programa \texttt{groups} nos permite saber a qué grupo/s pertenece un usuario:

\begin{bashcode}
groups     # Imprime los grupos a los que pertenece el usuario que
           # ejecutó la orden
groups foo # Imprime los grupos a los que pertenece el usuario foo
\end{bashcode}

Por supuesto, solamente el usuario \texttt{root} es capaz de administrar usuarios y grupos. Estos son algunos ejemplos:

\begin{bashcode}
useradd -g users foo # Crea un nuevo usuario llamado foo, cuyo grupo
                     # primario será users; users debe existir
groupadd audio       # Crea un nuevo grupo llamado audio
gpasswd -a foo audio # Agrega el usuario foo al grupo audio (audio
                     # será un grupo suplementario de foo)
gpasswd -d foo video # Quita ll usuario foo del grupo video
groupdel video       # Elimina el grupo video del sistema
userdel -rf bar      # Elimina el usuario bar del sistema; la opción
                     # -r hace que se elimine su directorio HOME y la
                     # opción -f hace que se elimine el usuario aunque
                     # esté logueado aún en el sistema
\end{bashcode}

\section{Permisos}

Cada archivo (regular o no) posee un usuario propietario, un grupo propietario y cuatro tipos de permisos:
Lectura, Escritura, Ejecución y Set ID.
Tres conjuntos de estos permisos están presentes: permisos para usuario, para grupo y para otros.
Solamente el usuario propietario puede modificar los permisos de un archivo.

Los permisos de lectura y escritura se explican solos: 

\begin{itemize}
\item Si un archivo posee activado el permiso de lectura (escritura) para usuario, entonces el usuario propietario podrá ver 
(modificar) el contenido del archivo.
\item Si un archivo posee activado el permiso de lectura (escritura) para grupo, entonces todo usuario que pertenezca al grupo 
propietario podrá ver (modificar) el contenido del archivo.
\item Si un archivo posee activado el permiso de lectura (escritura) para otros, entonces cualquier usuario del sistema 
podrá ver (modificar) el contenido del archivo.
\end{itemize}

Los permisos de ejecución se aplican sólo a archivos regulares y a directorios: 

\begin{itemize}
\item Si un archivo regular posee activado un permiso de ejecución, entonces todo usuario que cumpla con la categoría del 
permiso (usuario, grupo y otro) podrá ejecutar el archivo como programa.
\item Si un directorio posee activado un permiso de ejecución, entonces todo usuario que cumpla con la categoría del 
permiso (usuario, grupo y otro) podrá ingresar a este directorio (en otras palabras, podrá asignarlo directorio de trabajo --
hacer \texttt{cd <directorio>}).
\end{itemize}

\paragraph{Los bits SetID.} Los permisos SetID para usuario (SetUID) y para grupo (SetGID) tienen significado solamente en aquellos 
archivos regulares que poseen activados ciertos permisos de ejecución. Si un archivo tiene el permiso SetUID activado y un usuario 
tiene permiso de ejecución sobre este archivo, entonces al ejecutar este archivo el usuario tendrá, dentro del proceso en ejecución, 
todos los privilegios del usuario propietario del archivo. Asimismo, si un archivo tiene el permiso SetGID activado y un usuario 
tiene permiso de ejecución sobre este archivo, entonces al ejecutar este archivo el usuario tendrá, dentro del proceso en ejecución, 
todos los privilegios del grupo propietario del archivo.

\paragraph{El bit Sticky.} El permiso SetID para otros tiene el nombre especial ``sticky'' (pegajoso). Este permiso solamente 
tiene significado para directorios. Si un directorio tiene el permiso sticky activado, entonces todo archivo podrá ser modificado 
solamente por el usuario propietario del archivo en cuestión. Como resultado, si un directorio es sticky y dentro de este directorio 
existe un archivo cuyo propietario no coincide con el usuario propietario del directorio, entonces \textit{el usuario propietario del 
directorio no podrá eliminar el archivo, y por lo tanto tampoco podrá eliminar el directorio}, a no ser, por supuesto, que el propietario 
del directorio desactive el bit sticky en este directorio, cosa que puede hacer por ser el usuario propietario.

\alert{Si un directorio posee un permiso de escritura activado, todo archivo ubicado allí dentro podrá ser movido o eliminado
\emph{aún cuando estos archivos tengan desactivado el permiso de escritura}. Es muy fácil olvidar este hecho, lo cual en ciertos casos 
puede generar huecos de seguridad importantes.}

\section{Ver y modificar propietarios y permisos de archivos}

La opción \texttt{-l} del programa \texttt{ls} hace que se muestren los archivos ingresados como argumentos en forma detallada. La primera 
columna mostrará el tipo de erchivo y los permisos; las tercera y cuarta columna mostrarán el usuario propietario y el grupo propietario del 
archivo, respectivamente.

\begin{text}
% ls -l /usr/bin/
total 504
drwxr-xr-x   7 root root 135168 ago 31 19:41 bin
drwxr-xr-x   2 root root   4096 mar 27 20:32 etc
drwxr-xr-x   5 root root   4096 ago 30 23:06 i686-w64-mingw32
drwxr-xr-x 515 root root  69632 ago 31 19:40 include
drwxr-xr-x 241 root root 225280 ago 31 19:41 lib
drwxr-xr-x  25 root root  36864 ago 31 19:40 lib32
lrwxrwxrwx   1 root root      3 feb 15  2015 lib64 -> lib
drwxr-xr-x  13 root root   4096 jun 26 04:22 local
lrwxrwxrwx   1 root root      3 feb 15  2015 sbin -> bin
drwxr-xr-x 301 root root  12288 ago 26 00:58 share
drwxr-xr-x   4 root root   4096 ago 30 17:02 src
drwxr-xr-x   5 root root   4096 ago 30 23:06 x86_64-w64-mingw32
\end{text}

Veamos cómo se lee la primera columna. El primer carácter indica el tipo de archivo. Los tipos fundamentales de archivo existentes
en GNU+Linux son siete. La tabla siguiente muestra qué significa cada carácter.
\bigskip

\bigskip
{\noindent\colorbox{bg}{
	\begin{tabular}{C{.05\textwidth} p{.85\textwidth}}
- & Archivo regular \\
d & Directorio \\
l & Enlace simbólico \\
p & Fifo (pipa con nombre) \\
s & Socket \\
c & Dispositivo por carácter \\
b & Dispositivo por bloques \\
	\end{tabular}}}
\bigskip

A continuación vemos los permisos del archivo. El primer conjunto de tres caracteres indica los permisos de usuario, el segundo
conjunto indica los permisos de grupo y el tercer conjunto indica los permisos de otros. \texttt{r} indica permiso de lectura (read)
activado, \texttt{w} indica permiso de escritura (write) activado y \texttt{x} indica permiso de ejecución (execute) activado.
Un guión alto (\texttt{-}) indica que el permiso de la ubicación correspondiente está desactivado.

Los permisos SetID y Sticky reemplazan la \texttt{x} correspondiente. Los permisos SetID se ven con una \texttt{s} si el permiso de 
ejecución correspondiente está activado y con una \texttt{S} si el 
permiso de ejecución correspondiente está desactivado. El permiso Sticky se ve con una \texttt{t} si el permiso de ejecución 
para otros está activado y con una \texttt{T} si el permiso de ejecución para otros está desactivado.

\subsection{Modificar propietarios}

Al crear un archivo nuevo, el usuario propietario y grupo propietario del archivo serán el usuario que lo creó y el grupo primario 
de dicho usuario. Para cambiar el usuario o grupo propietario de un archivo usamos \texttt{chown}. Si un usuario desea cambiar el 
grupo propietario de un archivo, el usuario debe ser el usuario propietario del archivo y a la vez pertenecer al grupo que desea 
asignar. Solamente el usuario \texttt{root} puede cambiar el usuario propietario de un archivo.

El propietario a asignar con \texttt{chown} se escribe en la forma \texttt{usuario:grupo}. La opción \texttt{-R, -{}-{}recursive} hace 
que \texttt{chown} actúe recursivamente en directorios.

\begin{bashcode}
chown foo /home/foo/bar      # Asigna al usuario foo propietario del
                             # archivo bar.
chown foo: /home/foo/bar     # Asigna al usuario foo y al grupo
                             # primario de foo propietarios del
                             # archivo bar.
chown :users /home/foo/bar   # Asigna al grupo users propietario del
                             # archivo bar.
chown -R foo:users /home/foo # Asigna al usuario foo y grupo users el
                             # directorio /home/foo y todo su
                             # contenido, recursivamente
\end{bashcode}

Adicionalmente, el programa \texttt{chgrp} permite cambiar solamente grupos propietarios. La sintaxis es la misma que para \texttt{chown}.

\begin{bashcode}
chgrp root /home         # Asigna al grupo root propietario de /home
chgrp -R users /home/foo # Asigna al grupo users como propietario del
                         # directorio /home/foo, recursivamente
\end{bashcode}

\subsection{Modificar permisos}

Solo el usuario propietario de un archivo puede cambiar sus permisos. El programa usado para cambiar permisos es \texttt{chmod}.
La sintaxis de un permiso consiste en tres conjuntos caracteres. El primero indica si el permiso es para usuario (\texttt{u}), 
grupo (\texttt{g}), otros (\texttt{o}), una combinación de estos o todos ellos (\texttt{a}). El segundo conjunto es un carácter 
que indica si activamos (\texttt{+}) o desactivamos (\texttt{-}) el permiso. El tercer conjunto indica los tipos de permiso: 
\texttt{r} para lectura, \texttt{w} para escritura, \texttt{x} para ejecución, \texttt{s} para SetID, \texttt{t} para Sticky o
una combinación de estos.

Al igual que \texttt{chown}, \texttt{chmod} tiene la opción \texttt{-R, -{}-{}recursive} para actuar recursivamente en directorios.

\begin{bashcode}
chmod u+x foo.sh # Activa el permiso de ejecución para usuario en
                 # foo.sh
chmod go-w /home # Desactiva el permiso de escritura para grupos y
                 # para otros en /home
chmod -R a-w foo # Desactiva los permisos de escritura para todos
                 # (usuario, grupo y otros) en foo, recursivamente
chmod o-rwx bar  # Desactiva los permisos de lectura, escritura y
                 # ejecución para otros en bar
\end{bashcode}

\info{En Linux, la llamada de sistema \texttt{chmod} no permite cambiar permisos a enlaces simbólicos: los permisos en este
caso son aplicados al archivo al que apunta el enlace.

En el caso de un enlace físico, sí es posible manejar permisos específicos para cada enlace. Esto permite administrar distintos 
puntos de acceso a un archivo, cada uno de ellos con sus correspondientes permisos.}

También puede usarse el equivalente hexadecimal para asignar todos los permisos con un solo argumento. Este equivalente consiste
en un número de cuatro dígitos hexadecimales (0-7). El primer dígito es opcional, e indica SetID y Sticky, donde 1 indica Sticky, 
2 indica SetGID y 4 indica SetUID. Los últimos tres indican respectivamente los permisos de usuario, grupo y otros, donde 1 
significa permiso de ejecución, 2 significa permiso de escritura y 4 significa permiso de lectura. Algunos ejemplos:

\begin{bashcode}
chmod 1755 foo   # 1 = bit Sticky.
                 # 7 = 1 + 2 + 4 = todos los permisos para usuario.
                 # 5 = 1 + 4 = permisos de lectura y ejecución para
                 # grupo y otros.
chmod 700 /root  # 7 = 1 + 2 + 4 = todos los permisos para usuario.
                 # 0 = ningún permiso para grupo y otros.
chmod 644 bar    # 6 = 2 + 4 = permisos de lectura y escritura para
                 # usuario.
                 # 4 = permisos de lectura para grupo y otros.
chmod 744 foo.sh # 7 = 1 + 2 + 4 = todos los permisos para usuario.
                 # 4 = permisos de lectura para grupo y otros.
\end{bashcode}

\alert{Normalmente la llamada de sistema \texttt{chmod} no borra permisos SetUID y SetGID a no 
ser que se lo ordene explícitamente. Esto quiere decir que, si un archivo posee alguno de estos 
permisos activados y ejecutamos \texttt{chmod 0644}, esto no nos garantiza que los hayamos 
desactivado. Debemos hacerlo explícitamente mediante \texttt{chmod ug-s}.}

\reference{
whoami & Imprime el nombre del usuario & coreutils \\
passwd & Cambia la contraseña de un usuario & shadow \\
groups & Imprime los grupos de un usuario & shadow \\
useradd & Crea un nuevo usuario & shadow \\
groupadd & Crea un nuevo grupo & shadow \\
gpasswd & Administra /etc/group y /etc/gshadow & shadow \\
userdel & Elimina un usuario & shadow \\
groupdel & Elimina un grupo & shadow \\
ls & Lista el contenido de directorios & coreutils \\
chown & Cambia usuario y grupo propietario de archivos & coreutils \\
chgrp & Cambia el grupo propietario de archivos & coreutils \\
chmod & Cambia modos de permisos de archivos & coreutils \\
}
\seealso{su & Su y Sudo & Cómo acceder a la cuenta root desde otra cuenta. El programa sudo. \\
archivos & Manejo de archivos en consola & Ver, copiar, mover y eliminar archivos y directorios en consola. \\
enlaces & Enlaces & Enlaces físicos y enlaces simbólicos. Qué son y cómo se trabajan. \\}
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
