\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Enlaces}
\def\categoria{basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{13 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}
\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}
\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}
\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Enlaces físicos y enlaces simbólicos. Qué son y cómo se trabajan.
\end{abstract}

%\depends{nombre & título & objetivo \\} % Tutoriales necesarios para comprender el contenido.
Todo sistema operativo identifica un archivo por su ruta (path), es decir, su ubicación en su sistema
de archivos. El sistema de archivos de un sistema GNU+Linux comienza en el directorio raiz, llamado \texttt{/}.
Todos los archivos del sistema estarán entonces ubicados en algún directorio debajo de \texttt{/}. Por ejemplo, 
el archivo \texttt{/usr/bin} es el directorio que contiene todos (o casi todos) los programas que el usuario 
utiliza; el directorio \texttt{/home/foo} es el directorio personal (llamado \texttt{HOME}) del usuario foo; el 
archivo \texttt{/proc/meminfo} contiene la información que Linux brinda a tiempo real acerca de la memoria usada, 
etcétera.

El hecho es que, por varias razones, para organizar tanto la información del sistema como la información personal, 
resulta conveniente (y a veces necesario) que un archivo tenga más de una ruta asociada, de manera tal de poder 
acceder al mismo archivo (físico) a través de varias rutas. Por ejemplo, supongamos que por alguna razón varias 
herramientas utilizan la misma librería \texttt{foo.so}, pero una de ellas supone que está ubicada en 
\texttt{/usr/lib/foo.so}, otra supone que está en \texttt{/lib/foo.so} y por un tema de versiones otra asume que 
está en \texttt{/usr/lib/foo.so.2.1} y otra asume que está en \texttt{/usr/lib/foo.so.2.2}. Esta librería sabe que 
según su estándar una herramienta puede querer acceder a ella mediante varias rutas, pero no quiere copiar el mismo
archivo en varios lugares distintos, porque sería un desperdicio de espacio en disco. Entonces puede instalar la 
librería con su nombre de versión real (por ejemplo, \texttt{/usr/lib/foo.so.2.3.5}) y crear todos los enlaces a este 
archivo que crea necesarios.

En Linux, asignar una nueva ruta a un archivo significa crear, con la ruta deseada, un archivo que llamamos 
\textit{enlace}. Este enlace puede ser físico o simbólico.

\section{Enlaces físicos}

Cuando creamos un enlace físico, creamos un nuevo archivo cuyo contenido físico coincide con el del archivo que 
estamos enlazando. Esto quiere decir que ambas \textit{rutas} apuntan al mismo sector físico del dispositivo que los 
almacena (ya sea disco rígido, RAM, dispositivo de almacenamiento extraíble, etcétera). Es por eso que los enlaces físicos
tienen el limitante de que tanto el archivo enlazado como el archivo que lo enlaza deben estar almacenados en el mismo 
dispositivo. En el caso de un disco rígido, los archivos deben estar almacenados en la misma partición.

Una vez creado uno o varios enlaces físicos a un solo archivo, no hay nada que los diferencie a la hora de trabajar con ellos
(salvo la \textit{ruta}, claro). Esto implica que si luego eliminamos uno de estos enlaces (incluso el original), el contenido 
del archivo no se perderá, porque uno puede seguir accediendo al mismo a través de los demás enlaces. En otras palabras: 
\textit{Un archivo es en sí mismo un enlace físico; un archivo puede tener varios enlaces físicos siempre que estén en el mismo 
dispositivo; y el contenido de un archivo será irrecuperable solo cuando no haya enlaces físicos que apunten a él.}

\info{Para evitar lazos infinitos de directorios, Linux no permite crear enlaces físicos a directorios.}

El programa usado para crear enlaces se llama \texttt{ln}. En su forma más simple y más usada, el primer argumento será el 
\textit{path} del archivo que queremos enlazar, y el segundo argumento será el \textit{path} del enlace a crear (el cual no puede 
ya existir). Por ejemplo:

\begin{bashcode}
ln foo bar/foobar # Crea un nuevo archivo bar/foobar,
                  # el cual es un enlace físico a foo.
\end{bashcode}

\section{Enlaces simbólicos}

Un enlace simbólico es muy diferente de un enlace físico. Ahora el enlace creado no tiene la misma naturaleza que el archivo que 
enlaza, sino que \textit{un enlace simbólico es un tipo especial de archivo cuyo contenido es un texto arbitrario que luego el 
sistema resolverá cuando se quiera acceder a él}. Veamos qué significa esto.

El programa \texttt{ln} hará un enlace simbólico si le damos la opción \texttt{-s, -{}-{}symbolic}.

\begin{bashcode}
ln -s foo bar    # Crea un enlace simbólico a foo llamado bar
ln -s ../foo bar # Crea un enlace simbólico, llamado bar, que apunta
                 # al archivo foo ubicado en el directorio padre
\end{bashcode}

En ambos comandos del ejemplo, \texttt{bar} apunta a \texttt{foo}. En el primer comando, \texttt{bar} busca a \texttt{foo} en el 
mismo directorio donde él se encuentra; en el segundo comando, \texttt{bar} busca a \texttt{foo} en el directorio padre. En ambos casos,
si eliminamos \texttt{foo}, el contenido del archivo se perderá, porque el enlace simbólico creado solamente posee la ruta de \texttt{foo}
y no su contenido. Como resultado, el enlace simbólico apunta a un archivo que no existe y es por eso llamado \textit{enlace roto}.
Podemos incluso crear enlaces simbólicos rotos, creando un enlace que contenga un texto que no da como resultado la ruta de un archivo 
existente.

Ahora bien, ¿qué ocurre si movemos el enlace simbólico a otro directorio? En el primer comando del ejemplo el enlace \texttt{bar} seguramente 
estará apuntando a una ruta diferente porque el directorio relativo es otro. En el segundo comando del ejemplo el enlace \texttt{bar} apuntará 
al mismo archivo solamente si su directorio padre coincide con el anterior. Esto es deseable en muchos casos, pero no siempre. 
Si queremos que la ruta que enlaza se preserve, debemos dar la \textit{ruta absoluta} a la hora de crear el enlace.
\info{Se llama \textit{ruta absoluta} a una ruta que está referida al directorio raiz del sistema. Entonces, una ruta es absoluta si y 
sólo si comienza con el carácter \texttt{/}.}

\begin{bashcode}
ln -s /usr/bin/foo bar # Crea un enlace simbólico a /usr/bin/foo
ln -s /usr/bin/foo     # Lo mismo, pero ahora el enlace se llama foo
\end{bashcode}

En el ejemplo que dimos en la sección anterior, el enlace creado no apuntará al mismo archivo si en vez de crear un enlace físico creamos 
un enlace simbólico:

\begin{bashcode}
ln -s foo bar/foobar # Crea un enlace simbólico a foo en el directorio
                     # bar/, llamado foobar
\end{bashcode}

porque ahora el contenido de \texttt{foobar} es \texttt{foo}, entonces, como \texttt{foobar} está ubicado en el directorio \texttt{bar}, 
el enlace apuntará a un archivo llamado \texttt{foo} pero \textit{ubicado en el directorio bar}.

\reference{ln & Crea enlaces entre archivos & coreutils}
%\seealso{nombre & título & objetivo \\} % Referencia a tutoriales relacionados.
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
