\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox,twoopt}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Acerca de las categorías de estos tutoriales}
\def\categoria{meta}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{6 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}

\newcommandtwoopt{\tabulartwo}[3][.2\textwidth][.725\textwidth]{
	\bigskip\noindent\colorbox{bg}{
	\begin{tabular}{C{#1} p{#2}}
		#3
	\end{tabular}}\bigskip}

\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Qué significan las categorías que ya existen y cuáles son los criterios para agregar nuevas.
\end{abstract}

\depends{meta & Acerca de nuestros tutoriales GNU+Linux & En qué consisten estos tutoriales, cómo deben leerse y qué hacer para escribir nuevos.}

Nuestros tutoriales GNU+Linux están organizados por \textit{categorías}. Todo tutorial pertenecerá a una o varias categorías. 
Los objetivos principales de las categorías son:
\begin{enumerate}
\item Mantener los tutoriales organizados a medida que van creciendo en número.
\item Facilitar la búsqueda de un tutorial que pueda ser útil para resolver un problema dado.
\end{enumerate}

Si usted escribe un tutorial nuevo y cree que puede asignarle una categoría (ya existente o no) que aporte a estos dos objetivos,
entonces lo correcto es hacerlo. Si el tutorial no pertenece a ninguna de las categorías ya existentes, tómese el trabajo de pensar en 
una categoría nueva que sea realmente representativa del contenido del tutorial, y a la cual puedan pertenecer futuros nuevos tutoriales 
(sin ningún compromiso de escribirlos usted, por supuesto).

La siguiente tabla muestra las categorías de tutoriales que existen actualmente y sus significados.

	\tabulartwo{
		\rm\bf Nombre & \rm\bf Descripción \\
		admin & Temas que pueden ser útiles a la hora de actuar como el administrador de un sistema. En otras palabras,
			estos tutoriales explican una o más herramientas que suelen ser utilizadas como usuario \texttt{root}. \\
		avanzado & Tutorial que puede abarcar otra/s categoría/s pero a un nivel avanzado. Estos tutoriales deberían pedir al lector un conocimiento previo,
		al menos de aquellos tutoriales que pertenecen a la/s misma/s categoría/s, pero a la categoría \texttt{basico} en vez de a esta. \\
		gnu & Tutorial que abarca herramientas GNU que son esenciales para un sistema GNU+Linux. \\
		basico & Tutorial que puede abarcar otra/s categoría/s pero a un nivel básico. Estos tutoriales no deben pedir al lector un conocimiento previo,
		salvo de otros tutoriales que también pertenezcan a esta categoría. \\
		doc & Acerca de herramientas de documentación de un sistema GNU+Linux. \\
		guis & Temas relacionados a interfaces gráficas, gestores de ventana y/o entornos de escritorio. \\
		hardware & Manejo y configuración de dispositivos y sus controladores (drivers). \\
		herramientas & Acerca de una herramienta específica, no esencial para el sistema. \\
		meta & Acerca de estos tutoriales. \\
		distro & Acerca de distribuciones GNU+Linux y de la gestión de los paquetes de un sistema, ya sea en una o en varias distribuciones. \\
}

\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
