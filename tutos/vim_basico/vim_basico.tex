% Plantilla de Tutoriales GNU+Linux - Rama Estudiantil IEEE UNS
% Mariano López Minnucci <mlopezviedma@gmail.com> 9/10/2015
\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{El editor de texto Vim} % Colocar el título del tutorial aquí
\def\categoria{basico herramientas} % Colocar la/s categoría/s del tutorial aquí
\def\autor{Marcelo López Minnucci} % Colocar el nombre del autor aquí
\def\email{coloconazo@gmail.com} % Colocar el correo electrónico del autor aquí
\date{16 de octubre de 2015} % Colocar la fecha de la última edición para evitar que se sobreescriba

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Editar archivos de texto en la consola utilizando Vim.
\end{abstract}
%\depends{nombre & título & objetivo \\} % Tutoriales necesarios para comprender el contenido.

Vim es un editor de texto para la consola de Linux; si
bien se trata de un software muy potente y versátil, dominar sus principios y
comandos básicos es muy sencillo, y nos permitirá realizar las tareas de
edición necesarias para administrar nuestro sistema GNU/Linux.

Como es usual cuando se trata de software libre, la documentación del
programa es muy completa y fácilmente accesible; este tutorial sólo pretende
iluminar sus conceptos más elementales.

\section{Primeros pasos}

Comencemos por invocar el editor abriendo el archivo \texttt{prueba.txt}:

\begin{bashcode}
vim prueba.txt
\end{bashcode}

En línea inferior vemos un mensaje del editor, que nos indica el nombre del
archivo y la posición del cursor en el mismo.

En este punto, el editor se halla en \emph{modo normal}. Para ingresar un texto,
primero debemos entrar al \emph{modo edición}; pulse la tecla \texttt{i} (verá 
en la línea inferior el mensaje ``\textbf{\texttt{-{}- INSERTAR -{}-}}'', que nos indica que 
estamos en modo edición).

Ingrese ahora algunas líneas de texto; para volver al modo normal, pulse 
\texttt{Esc}.

La tecla \texttt{:} (dos puntos) se utiliza para ejecutar comandos en el
editor; para guardar el archivo, presione entonces \texttt{:} (verá el
cursor en la línea inferior), luego \texttt{w} (el comando utilizado para
guardar el archivo, de \emph{write}, escribir) y a continuación \texttt{Enter}.

Luego de guardar el archivo, teclee \texttt{:q[Enter]} para salir del editor.
(Para guardar el archivo y salir del editor con un mismo comando, podemos
teclear \texttt{:wq[Enter]}.)

\info{Si bien las teclas de cursor pueden utilizarse, es recomendable habituarnos
a utilizar las teclas de movimiento típicas de \texttt{vim}:
\texttt{h},
\texttt{j},
\texttt{k} y
\texttt{l} para izquierda, abajo, arriba y derecha, respectivamente, ya que nos
permiten trabajar con más rapidez y comodidad.}

\section{Invocar el editor}

A continuación se muestran algunas maneras útiles de llamar al editor:

\begin{bashcode}
vim archivo1 archivo2 ... # Abre varios archivos simultáneamente
vim -R archivo.txt        # Abre el archivo como sólo lectura (útil
                          # para ver archivos sin riesgo de
                          # modificarlos por accidente)
view archivo.txt          # Sinónimo de vim -R
vim +5 archivo.txt        # Abre el archivo y sitúa el cursor en la
                          # línea 5
vim +/texto               # Abre el archivo y sitúa el cursor en la
                          # primera ocurrencia de la expresión 'texto'
\end{bashcode}

\section{Comandos y teclas útiles}

A continuación se describen los comandos y teclas más usados en la edición de
archivos; es recomendable crear un archivo de prueba y utilizarlo para
practicar hasta que nos resulte automático usarlos.

Observe que varios comandos pueden estar precedidos por un número, que será
interpretado de diferente forma según de qué comando se trate: copiar
\emph{n} líneas, borrar \emph{n} caracteres, etc.

\bigskip
{\noindent\colorbox{bg}{
	\begin{tabular}{C{.3\textwidth} p{.6\textwidth}}
		\rm\bf Secuencia de teclas & \rm\bf Acción realizada \\
i & Entrar al modo edición \\
o & Insertar nueva línea y entrar al modo edición \\
Ins & En modo edición, alternar entre \emph{insertar} y \emph{sobreescribir}
      texto \\
Esc & Volver al modo normal \\
w & Mover el cursor a la siguiente palabra \\
\^{} & Mover el cursor al comienzo de la línea \\
\$ & Mover el cursor al final de la línea \\
dd & Borrar una línea de texto y almacenarla en el buffer (``cortar'' texto) \\
5dd & Cortar 5 líneas \\
x & Cortar un carácter \\
3x & Cortar 3 caracteres \\
dw & Cortar una palabra \\
d12w & Cortar 12 palabras \\
yy & Copiar una línea \\
5yy & Copiar 5 líneas \\
p & Pegar el contenido del buffer en la posición del cursor \\
3p & Pegar 3 veces el contenido del buffer \\
/ & Buscar texto: teclear \texttt{/}, luego el texto a buscar, por último
    \texttt{Enter} \\
n & Ir a la siguiente ocurrencia de la palabra buscada \\
N & Ir a la ocurrencia anterior de la palabra buscada \\
u & Deshacer la última acción realizada \\
C-r & Rehacer la última acción deshecha \\
	\end{tabular}}}
\bigskip

Las acciones que se muestran a continuación van precedidas por la tecla
\texttt{:}, que como hemos visto se utiliza para ejecutar comandos
específicos en el editor. Para esto, pulse la tecla \texttt{:}, teclee el
comando y pulse \texttt{Enter}.

\bigskip
{\noindent\colorbox{bg}{
	\begin{tabular}{C{.3\textwidth} p{.6\textwidth}}
		\rm\bf Comando & \rm\bf Acción realizada \\
:17 & Ir a la línea 17 \\
:+12 & Avanzar 12 líneas \\	
:-12 & Retroceder 12 líneas \\
:next :n & Al editar varios archivos simultáneamente, ir al siguiente
           archivo \\
:previous :p & Ir al archivo anterior \\
:first & Ir al primer archivo \\
:last & Ir al último archivo \\
:wnext :wn & Guardar los cambios e ir al siguiente archivo \\
:wprevious :wp & Guardar los cambios e ir al archivo anterior \\
:set & Mediante este comando es posible configurar el comportamiento del
       editor según las preferencias del usuario; existen muchísimas
		 variables que pueden ser configuradas. A continuación se muestran
		 algunos ejemplos. \\
:set autoindent & Activar auto--indentación \\
:set noautoindent & Desactivar auto--indentación (en general, :set noVARIABLE
                    desactiva la variable en cuestión) \\
:set number & Muestra los números de línea \\
:set relativenumber & Muestra los números de línea relativos a la ubicación
                      del cursor \\
:set tabstop=3 & Ajusta la tabulación a 3 espacios (por defecto es 8) \\
:w & Guardar los cambios en el archivo \\
:q & Salir del editor \\
:wq & Guardar los cambios y salir \\
:q! & Salir sin guardar los cambios \\
:help & Explorar las páginas de ayuda; \texttt{:help TEMA} muestra la ayuda
        para un tema en particular. Para volver al editor use \texttt{:q} \\
:help! & Muestra un texto de ayuda para situaciones especialmente problemáticas \\
	\end{tabular}}}
\bigskip

\info{Sobre todo si no estamos habituados a utilizar este tipo de editores,
es necesaria una cierta práctica para editar archivos con comodidad y
rapidez. Es recomendable comenzar con el tutorial incluído en la distribución
de \texttt{vim}: simplemente ejecute el comando \texttt{vimtutor} y siga las
instrucciones.}

\info{Ciertos programas invocan un editor de texto durante su ejecución; si
deseamos configurar \texttt{vim} como el editor invocado por defecto, podemos
exportar la variable \texttt{EDITOR} en el entorno de ejecución del intérprete
de comandos. Para el caso de \texttt{bash}, agregamos a nuestro \texttt{.bashrc}
la siguiente línea:

\texttt{export EDITOR=vim}

}

% Contenido del tutorial
% \section{Una sección}
% \subsection{Una subsección}
% \paragraph{Un párrafo con título.} Ejemplo de párrafo.
% \info{Esto es un cuadro informativo}
% \alert{Esto es un cuadro de alerta}
% \begin{bashcode}
% # Esto es un código de bash.
% \end{bashcode}
% \begin{text}
% Recuadro para mostrar textos planos.
% \end{text}
% \begin{textn}
% Recuadro para mostrar textos planos
% con números de líneas a la izquierda.
% \end{textn}
\reference{vim & Editor de archivos de texto para la consola & vim / vim-minimal \\} % Referencia de todos los programas usados y explicados.
%\seealso{nombre & título & objetivo \\} % Referencia a tutoriales relacionados.
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
