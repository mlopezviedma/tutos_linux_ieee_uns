\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Manejo de archivos en consola}
\def\categoria{basico gnu}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{6 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Ver, copiar, mover y eliminar archivos y directorios en consola.
\end{abstract}

\section{Directorio de trabajo}

Un usuario siempre está ubicado en un determinado directorio, llamado \emph{directorio de trabajo}.
Para saber en qué directorio estamos, podemos usar el comando \texttt{pwd}\footnote{\texttt{pwd} es
también una función interna de bash.} (print working directory):

\begin{bashcode}
pwd # Imprime el directorio actual.
\end{bashcode}

Los directorios en GNU+Linux no son más que un tipo especial de archivo que contiene una lista de 
entradas que pueden ser archivos e inclusive directorios. A su vez, un directorio posee dos entradas
especiales: \texttt{.} (punto) el cual apunta al mismo directorio; y \texttt{..} (punto punto) que
apunta al directorio padre.

Para movernos a un directorio particular usamos la función interna de Bash\footnote{Cuando usamos el término
\emph{consola} nos referimos a un intérprete de comandos, el cual consiste en un lenguaje de programación
interpretado (es decir, pensado para que uno pueda ejecutar instrucciones en este lenguaje de forma
interactiva). Bash es uno de los tantos intérpretes de comandos existentes en GNU+Linux, y es
actualmente con el que prácticamente todas las distribuciones deciden trabajar.} \texttt{cd}:

\begin{bashcode}
cd foo/bar   # Ir al directorio foo/bar.
cd ..        # Ir al directorio padre.
cd ../foobar # Ir al directorio foobar, el cual está en el
             # directorio padre.
cd           # Ir al directorio HOME.
\end{bashcode}

\info{\texttt{HOME} (hogar) es el único directorio del sistema cuyo dueño es un usuario regular. Cada usuario siempre 
usará este directorio para trabajar con su propia información, porque será el único lugar en el sistema en el cual el 
usuario en cuestión es el único que posee permanentemente permisos de lectura y escritura. Normalmente están ubicados 
en \texttt{/home} y tienen el mismo nombre que el usuario correspondiente (por ejemplo, \texttt{/home/mariano}).}

\section{Ver el contenido de un directorio}

Bien, ya sabemos qué es un directorio y qué es el directorio de trabajo, pero ¿qué hay ahí? El programa \texttt{ls}
(list) lista los directorios y archivos que ingresamos como argumentos. Si un argumento es un directorio, \texttt{ls}
listará su contenido. Sin argumentos, \texttt{ls} listará el contenido del directorio de trabajo.

\begin{bashcode}
ls   # Lista el contenido del directorio de trabajo actual
ls ~ # Lista el contenido del directorio HOME
ls / # Lista el contenido del directorio raiz del sistema
\end{bashcode}

En bash, el símbolo \texttt{\~} es una abreviatura de nuestro directorio \texttt{HOME}. El directorio \texttt{/} es un 
directorio muy especial: es el directorio raiz del sistema. Esto significa que todo ente del sistema en ejecución estará 
ubicado en algún directorio debajo de este. En otras palabras, \texttt{/} es el único directorio que no posee directorio 
padre.

\info{Como en todo lenguaje de programación, el espacio es un carácter especial. En el caso de los intérpretes
de comandos, el espacio normalmente separa argumentos. Si un archivo o directorio posee espacios, la manera de ingresarlos
es comentando estos espacios, ya sea con una barra invertida:
\texttt{archivo\textbackslash{} con\textbackslash{} espacios} o encomillando todo el argumento:
\texttt{''archivo con espacios''}.
}

El programa \texttt{ls} posee varias opciones. Una opción muy cómoda es \texttt{-l}, la cual imprime la lista en forma
detallada. La opción \texttt{-h, -{}-{}human-readable} muestra los tamaños de los archivos con sufijos K, M, G, etcétera.

\begin{bashcode}
ls -l /  # Lista el contenido del directorio raiz en forma detallada
ls -lh foo/ # Lista el contenido del directorio foo mostrando los
            # tamaños de los archivos en forma más amigable
\end{bashcode}

Por convención, todos los archivos cuyo nombre comienza con un punto son considerados archivos ocultos. Esto no significa que 
estos archivos sean para nada especiales, sino que simplemente la mayoría de los programas no los muestran a no ser que uno 
les ordene lo contrario. Si queremos que \texttt{ls} liste también los archivos ocultos, usamos la opción 
\texttt{-A, -{}-{}almost-all}. Si usamos la opción \texttt{-a, -{}-{}all}, se listarán inclusive las entradas 
\texttt{''.''} y \texttt{''..''}.

\begin{bashcode}
ls -A ~           # Lista el contenido del directorio HOME
                  # incluyendo los archivos ocultos
ls --almost-all ~ # Lo mismo, pero usando la opción larga
\end{bashcode}

\section{Manipulación de archivos y directorios}

\paragraph{Comodín.} El carácter \texttt{*} es un comodín en bash que significa ``todos''.

\begin{bashcode}
ls *       # Lista todos los archivos ubicados en el directorio actual
ls *.txt   # Lista todos los archivos cuyo nombre termina en .txt
ls foo*    # Ídem, cuyos nombres comienzan con foo
ls foo*bar # Ídem, cuyos nombres comienzan con foo y termina con bar
\end{bashcode}

\info{Todos los programas que se explican a continuación tienen la opción en común \texttt{-v, -{}-{}verbose}, la cual hará
que el programa imprima en pantalla toda tarea que realiza. Es recomendable usarla, más aún cuando uno está aprendiendo
a trabajar con estas herramientas.}

\subsection{Copiar}

El programa \texttt{cp} es el encargado de copiar archivos y directorios. El último argumento dado será siempre el
archivo o directorio destino. En el caso de dar más de dos argumentos, entonces el destino deberá ser un directorio.

\begin{bashcode}
cp foo bar # Crea una copia del archivo foo cuyo nombre será bar.
           # Si bar ya existe y es un archivo, se sobreescribe.
           # Si bar ya existe y es un directorio, se crea una
           # copia dentro del directorio bar (bar/foo).
\end{bashcode}

Si no queremos sobreescribir archivos por accidente, la opción \texttt{-i} dirá a \texttt{cp} que trabaje en forma
interactiva (preguntará al usuario antes de sobreescribir). La opción \texttt{-n} evita que se sobreescriban archivos,
y la opción \texttt{-f} hace que siempre se sobreescriban archivos. \par
Para copiar directorios, debemos indicar a \texttt{cp} que copie recursivamente con la opción \texttt{-r}.

\begin{bashcode}
cp -n foo bar foobar/baz # Copia foo y bar dentro de foobar/baz
                         # sin sobreescribir archivos existentes
cp -r foo ../bar         # Si foo es un directorio, copia su contenido
                         # recursivamente en ../bar (si ../bar no
                         # existe, será el nombre de la nueva copia)
cp -r * ..               # Crea una copia de todos los archvos y
                         # directorios ubicados en el directorio actual
                         # en el directorio padre
\end{bashcode}

\subsection{Mover}

El programa \texttt{mv} es muy similar a \texttt{cp}, salvo que todos los archivos y directorios fuente son reemplazados
por los archivos y directorios destino. En este sentido, \emph{mover} y \emph{renombrar} son para Linux tareas equivalentes.

\begin{bashcode}
mv -n foo bar foobar/baz # Mueve foo y bar dentro de foobar/baz
                         # sin sobreescribir archivos existentes
mv -r foo ../bar         # Si foo es un directorio, mueve su contenido
                         # recursivamente en ../bar (si ../bar no
                         # existe, será su nuevo nombre)
\end{bashcode}

\subsection{Borrar}

Para borrar archivos del sistema usamos \texttt{rm}.

\alert{Tener en cuenta que un intérprete de comandos no maneja ``papeleras''.
En otras palabras, todo archivo que borremos con \texttt{rm} no podrá ser recuperado en el futuro.}

La lógica de \texttt{rm} es muy simple: elimina todo archivo ingresado como argumento. Para eliminar directorios, tenemos que
indicar a \texttt{mv} que trabaje recursivamente dando la opción \texttt{-r}. La opción \texttt{-i} hace que \texttt{mv} pregunte
antes de eliminar cada archivo.

\begin{bashcode}
rm foo bar     # Elimina foo y bar, si es que no son directorios
rm -ri foo bar # Elimina foo y bar recursivamente, preguntando antes
               # de eliminar cada archvo
rm -r *        # Elimina todos los archivos y directorios ubicados en
               # el directorio actual recursivamente
\end{bashcode}

\subsection{Directorios}

Para crear directorios nuevos usamos el programa \texttt{mkdir}. La opción \texttt{-p, -{}-{}parents} hace que se creen 
todos los directorios padre que todavía no existen.

\begin{bashcode}
mkdir foo        # Crea el directorio foo
mkdir foo/bar    # Crea el directorio bar dentro de foo
                 # (foo debe existir)
mkdir -p foo/bar # Si foo no existe, se crea y luego se crea el
                 # directorio bar
\end{bashcode}

El programa \texttt{rmdir} elimina directorios siempre y cuando estén vacíos. Esto es muy útil, especialmente si uno se hace la
costumbre de usar esta herramienta siempre que quiera eliminar un directorio que uno ``supone'' que está vacío. De esta manera
se evita toda pérdida de información no deseada, porque si un directorio no está vacío \texttt{rmdir} no hará nada con él.

\begin{bashcode}
rmdir foo bar        # Elimina los directorios foo y bar, omitiendo
                     # aquellos que no estén vacíos
rmdir -p foo/bar/baz # si foo/ solamente contiene bar/ y bar/
                     # solamente contiene baz/, se eliminan
                     # recursivamente
rmdir foo/bar/baz foo/bar foo # Equivalente al comando anterior
\end{bashcode}

\reference{
cd & Cambia el directorio de trabajo & - \\
pwd & Imprime el directorio de trabajo actual & coreutils \\
ls & Lista el contenido de directorios & coreutils \\
cp & Copia archivos y directorios & coreutils \\
mv & Mueve (renombra) archivos & coreutils \\
rm & Elimina archivos o directorios & coreutils \\
mkdir & Crea directorios & coreutils \\
rmdir & Elimina directorios vacíos & coreutils \\
}
%SEEALSO bash
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
