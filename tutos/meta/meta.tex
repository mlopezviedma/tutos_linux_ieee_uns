\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Acerca de estos tutoriales}
\def\categoria{meta}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{6 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
\end{tabular}}
\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
#1
\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
#1
\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}
\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}
\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
En qué consisten estos tutoriales, cómo deben leerse y qué hacer para escribir nuevos.
\end{abstract}

\section{El proyecto}

La idea de este proyecto es proveer una documentación concisa relacionada con GNU+Linux
para que el usuario que encuentre dificultad en un tema determinado pueda acudir a ella
y encontrar rápidamente una solución concreta y explicada a su problema. \par
Esta documentación está formada por pequeños tutoriales, donde cada uno de ellos aborda
una cuestión específica. De esta manera no es necesario que el usuario deba encontrarse
con una lectura extensa y necesitar filtrar demasiada información solamente para
solucionar un problema pequeño. \par
Acceda a 
\url{http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/all_in_one.pdf}
para ver \textbf{todos los tutoriales en un solo archivo PDF}. \par
Toda vez que surja una nueva dificultad, se proveerá el tutorial correspondiente.
Si dicho tutorial no existe, se redacta en el momento y se agrega al proyecto.
\paragraph{¿Cómo sé si ya hay un tutorial que resuelva mi problema?}\sloppy Mantenemos
una lista completa de nuestros tutoriales en el archivo \texttt{README.md} del proyecyo
(\url{http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/}), cada uno de ellos con su 
título, objetivo y categoría a la que pertenece.

\section{¿Cómo leo un tutorial?}

Antes que nada, asegúrese que el objetivo del tutorial coincida con la información que está
buscando. Dado que estos textos son extremadamente concisos, no habrá en cada tutorial mucho
más que lo que el objetivo indica. \par
Al comienzo de cada tutorial (a continuación del objetivo) se listan los tutoriales que uno
debe leer antes para poder comprender el contenido del mismo, de haber alguno. \par
Los comandos que el tutorial explica o sugiere ejecutar estarán mostrados en un cuadro como el
siguiente:

\begin{bashcode}
echo "Bienvenidos!" # Imprime un mensaje en pantalla.
\end{bashcode}

Información adicional será mostrada en un cuadro como el siguiente:

\info{Este es un cuadro informativo. Puede omitirse en una lectura rápida.}

Si la información prevista es importante o hay que tener un cuidado especial, se mostrará como
advertencia:

\alert{Nunca ignore cuadros como este.}

\paragraph{foobar.} A la hora de mostrar ejemplos, es normal que se acuda a nombres ficticios
como ``foo'', ``bar'', ``foobar'', ``baz'' y otros. Estos pueden hacer referencia a cualquier ente
informático: archivos, programas, paquetes, etcétera. \par
Al final de cada tutorial (o de una sección particular) se mostrará una referencia a tutoriales
relacionados y una referencia a los programas usados y explicados. El usuario puede profundizar
aún más en estos programas acudiendo a sus páginas de manual oficial.
Si usted ya posee instalado el paquete que provee dicho programa, puede ver la
página de manual correspondiente ejecutando

\begin{bashcode}
man foo # Accede a la página de manual del programa foo.
\end{bashcode}

\section{¿Cómo escribo un tutorial?}

Queremos mantener una misma estructura para todos los tutoriales. De esta manera, a los usuarios
les resultará  fácil familiarizarse con un tutorial nuevo. Es por eso que, si usted desea
redactar un tutorial para agregarlo a nuestra librería, se sugiere fuertemente
respetar las siguientes instrucciones.

\paragraph{Colaborando con el proyecto.} Tanto para mejorar los tutoriales que ya existen como para agregar nuevos,
lo ideal es colaborar a través del proyecto GIT. Clone el proyecto mediante

\begin{bashcode}
git clone http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns.git
\end{bashcode}

Cree una nueva rama (branch) para el proyecto cuyo nombre sea representativo de lo que hará,
como por ejemplo:

\begin{bashcode}
git checkout -b mount # Creación de un nuevo tutorial llamado mount; ó
git checkout -b bash+procesos # Agregado de información acerca de
                              # procesos al tutorial llamado bash
\end{bashcode}

Una vez que terminó con su trabajo, actualice los cambios en su proyecto local
(por favor, describa correctamente lo que hizo a través del mensaje del commit):

\begin{bashcode}
git add <archivos_nuevos> # Necesario si se agregaron nuevos archivos
git commit -m "Detalle del trabajo hecho"
\end{bashcode}

Finalmente, suba los cambios al proyecto remoto:

\begin{bashcode}
git push -u origin <nombre_de_la_rama>
\end{bashcode}

Los encargados de mantener el proyecto recibirán una notificación acerca de su trabajo, lo revisarán,
y si está todo en orden lo agregarán a la rama principal (\texttt{master}).

\paragraph{Nuestra plantilla.} Si desea escribir un tutorial nuevo, use nuestra plantilla \LaTeX{} 
de tutoriales ubicada en el directorio \texttt{plantilla/plantilla\_tuto/plantilla} del proyecto.
Copie este directorio dentro de \texttt{tutos} con el nombre del tutorial. Puede también usar el 
script \texttt{tutos\_manager.sh} del proyecto para hacer esto automáticamente 
(ejecute la siguiente orden en el directorio raiz del proyecto):

\begin{bashcode}
./tutos_manager -n <nombre_del_nuevo_tutorial>
\end{bashcode}

Reemplace las variables \texttt{\bfseries titulo}, \texttt{\bfseries categoria},
\texttt{\bfseries autor}, \texttt{\bfseries email} y \texttt{\bfseries date} en la cabecera del
archivo y provea un objetivo conciso del tutorial en \texttt{\bfseries abstract}\footnote{No
utilice el carácter | (barra vertical) en el título ni en el objetivo. Usamos este carácter
para separar estos campos en la base de datos de tutoriales.}.
Más allá de esto, no cambie nada de lo que ya está escrito. Las líneas comentadas están
allí como referencia de los comandos que debe usar para agregar cuadros de código, cuadros
informativos y cuadros de advertencia, así como también tablas de tutoriales necesarios,
tablas de referencias a programas y tablas de referencias a otros tutoriales. \par
\sloppy Para más información, puede acudir a los tutoriales ya existentes en el directorio
\texttt{tutos} para usarlos como modelos.

Si desea trabajar independientemente del proyecto GIT, puede descargar la plantilla desde
\url{http://gitlab.com/mlopezviedma/tutos_linux_ieee_uns/raw/master/plantilla/plantilla_tuto.zip}
o pedirla personalmente, y trabajar libremente.

\paragraph{Comentarios.} En lo posible, agregue un comentario a la derecha de cada comando
sugerido como los ejemplos de la sección anterior. De esta manera el usuario que copie
el comando y lo ejecute en consola conservará el comentario en su historial de comandos.

\paragraph{Referencias a programas.} Coloque \emph{todos} los programas usados en el tutorial en
las referencias al final del tutorial (o de cada sección). El usuario debe contar con esto a la hora
de querer acceder a las páginas de manual correspondientes y, por qué no, a la documentación de los
paquetes a los que pertenecen.

\paragraph{Enlaces.} Para colocar enlaces utilice la instrucción \verb#\url{}#. De esta manera el
lector podrá acceder a la página web correspondiente simplemente haciendo click desde el documento.

\paragraph{\texttt{DOC.md}.} El archivo \texttt{DOC.md} muestra una documentación muy breve 
del proyecto, junto con una lista de tutoriales pendientes/propuestos. Siéntase libre de modificarlo 
en caso de ser conveniente.

\paragraph{Compilando el tutorial.} Si desea compilar el tutorial usted mismo para ver el resultado
en formato PDF, tenga en cuenta que es necesario tener instalada la librería de \emph{Python} 
\texttt{pygments} y el paquete de \LaTeX{} \texttt{minted}. Además, para compilar el tutorial use 
\texttt{pdflatex -shell-escape}.
Puede también usar el script \texttt{tutos\_manager.sh} del proyecto para compilar un tutorial
(ejecute la siguiente orden en el directorio raiz del proyecto):

\begin{bashcode}
./tutos_manager -m <nombre_del_tutorial> 
\end{bashcode}

\paragraph{Actualizando el proyecto.} Actualice la base de datos de los tutoriales, el archivo 
\texttt{README.md} y el documento PDF con todos los tutoriales:

\begin{bashcode}
./tutos_manager -g # Generar la base de datos de tutoriales
./tutos_manager -U # Actualizar el archivo README.md
./tutos_manager -a # Generar el documento all_in_one.pdf
\end{bashcode}

\seealso{categorias & Acerca de las categorías de nuestors tutoriales GNU+Linux & Qué significan las categorías que ya existen y cuáles son los criterios para agregar nuevas.}
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
