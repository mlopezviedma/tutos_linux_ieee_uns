\documentclass[a4paper]{article}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times,textcomp,gensymb,graphicx,fancyhdr,array,etoolbox}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage[cachedir=.minted]{minted}
\BeforeBeginEnvironment{minted}{\bigskip}\AfterEndEnvironment{minted}{\bigskip}
\BeforeBeginEnvironment{abstract}{\bigskip\hrule\bigskip}\AfterEndEnvironment{abstract}{\hrule\bigskip}

\def\titulo{Crear un usuario de prueba}
\def\categoria{basico}
\def\autor{Mariano López Minnucci}
\def\email{mariano.lopezminnucci.ar@ieee.org}
\date{24 de septiembre de 2015}

\usepackage[hidelinks,pdfauthor={\autor \email},pdfcreator={\autor},pdftitle={\titulo},
	colorlinks=true,linkcolor=black,urlcolor=blue,citecolor=black,filecolor=black]{hyperref}
\title{\titulo \\ \normalsize[ \categoria{} ]}\author{\autor \\ \ttfamily <\email>}
\definecolor{bg}{rgb}{0.93,0.93,0.93}
\definecolor{infobg}{rgb}{0.75,0.75,1}
\definecolor{alertbg}{rgb}{1,1,0.5}
\newminted{bash}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[text]{text}{tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\newminted[textn]{text}{linenos,tabsize=2,bgcolor=bg,formatcom=\bfseries,fontsize=\footnotesize}
\renewcommand{\theFancyVerbLine}{\ttfamily\textcolor[rgb]{0.4,0.4,0.4}{\scriptsize{\arabic{FancyVerbLine}}}}
\newcommand{\info}[1]{\begin{center}\colorbox{infobg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../info.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcommand{\alert}[1]{\begin{center}\colorbox{alertbg}{
	\begin{tabular}{p{.1\textwidth} p{.8\textwidth}}
	\includegraphics[height=32pt]{../alert.pdf}&\vspace{-32pt}#1
	\end{tabular}}
	\end{center}}
\newcolumntype{C}[1]{>{\ttfamily}m{#1}}
\newcommand{\reference}[1]{
	\section*{Comandos usados}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.5\textwidth} C{.2\textwidth}}
		\rm\bf Nombre & \rm\bf Descripción & \rm\bf Paquete \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\depends}[1]{
	\section*{Tutoriales necesarios}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}
\newcommand{\seealso}[1]{
	\section*{Véase también}\noindent\colorbox{bg}{\footnotesize
	\begin{tabular}{C{.2\textwidth} p{.3\textwidth} p{.4\textwidth}}
		\rm\bf Nombre & \rm\bf Título & \rm\bf Objetivo \\
		#1
	\end{tabular}}\bigskip}

\begin{document}
\renewcommand{\abstractname}{Objetivo del tutorial}\lhead{\titulo}
\rhead{\includegraphics[height=25pt]{../tux.pdf}\includegraphics[height=25pt]{../logorama.pdf}}
\pagestyle{fancy}\maketitle
\begin{center}
\includegraphics[height=50pt]{../tux.pdf}\includegraphics[height=50pt]{../logorama.pdf}
\end{center}
\begin{abstract}
Cómo crear y usar un usuario de prueba para probar nuevas cosas en el sistema sin riesgo de perder información
o dañar el sistema.
\end{abstract}

\depends{su & Su y Sudo & Cómo acceder a la cuenta root desde otra cuenta. El programa sudo. \\}

Muchos de estos tutoriales explican conceptos que se asimilan mejor probando algunas cosas manualmente.
Hay que tener presente entonces que al efectuar pruebas en nuestro sistema operativo corremos el riesgo de
dañarlo o perder información. Este tutorial propone una manera simple de evitar que esto ocurra.

La idea es crear un usuario de prueba y siempre trabajar con él cuando querramos probar cosas nuevas.
Linux será el encargado de evitar que perdamos información o dañemos nuestro sistema a través de su mecanismo de 
permisos multiusuario.

\section{Crear un usuario de prueba}

Antes que nada, elijamos un nombre para nuestro usuario de prueba. En este tutorial lo llamaremos \texttt{prueba}.
Entonces, antes de crear nuestro usuario, comprobemos que no existe ya en el sistema un usuario \emph{ni} un grupo con 
ese nombre.

\begin{bashcode}
grep ^prueba: /etc/passwd # Filtra el usuario del sistema "prueba"
grep ^prueba: /etc/group  # Filtra el grupo del sistema "prueba"
\end{bashcode}

Si la primera línea no imprime nada, significa que no existe un usuario \texttt{prueba} en el sistema.
Si la segunda línea no imprime nada, significa que no existe un grupo \texttt{prueba} en el sistema.
Para que este método sea efectivo, es importante que \emph{ninguna de estas órdenes imprima nada} (normalmente
este será el caso).

Por supuesto, para crear un usuario \texttt{prueba}, este no puede ya existir, pero ¿por qué comprobamos que no exista
tampoco el grupo? Pues, de esta manera nos aseguramos que el nuevo usuario a crear no tenga privilegios de ningún tipo
sobre el sistema, porque si el grupo no existe, entonces ningún archivo del sistema pertenecerá a este grupo tampoco.

Por supuesto, si nuestro nombre no cumple con estas condiciones, repetimos el proceso con un nuevo nombre.

Ahora sí, con la cuenta \texttt{root} creamos nuestro usuario:

\begin{bashcode}
useradd -Um prueba # Crea un nuevo usuario "prueba" cuyo grupo primario
                   # es "prueba" y cuyo directorio HOME es /home/prueba
\end{bashcode}

Esta orden creará un nuevo usuario \texttt{prueba}. La opción \texttt{-U, -{}-{}user-group} hace que se cree un nuevo grupo
con el mismo nombre que el nombre de usuario, y se le asigne este grupo al nuevo usuario. La opción \texttt{-m, -{}-{}create-home}
hace que se cree un directorio \texttt{HOME} cuya ruta es \texttt{/home/prueba}.

A continuación, también como \texttt{root}, le daremos una contraseña a esta cuenta.

\begin{bashcode}
passwd prueba # Establecer una contraseña para el usuario prueba
\end{bashcode}

¡Listo! Nuestro nuevo usuario está ahora disponible.

\section{Loguearse como nuestro usuario de prueba}

Ahora, cada vez que querramos trabajar con nuestro usuario de prueba, nos loguearemos desde cualquier consola y desde 
cualquier usuario:

\begin{bashcode}
su - prueba
\end{bashcode}

En estas condiciones, solamente podremos dañar aquellos archivos y procesos que pertenecen a este usuario de prueba.
Cuando hayamos terminado de usar esta cuenta, salimos:

\begin{bashcode}
exit
\end{bashcode}

\reference{grep & Imprime líneas siguiendo un patrón & grep \\
useradd & Crea un nuevo usuario & shadow \\
passwd & Cambia la contraseña de un usuario & shadow \\
su & Cambiar de usuario & util-linux \\
}

\seealso{permisos & Usuarios, grupos y permisos & Cómo trabajan los permisos de archivos para los distintos usuarios y grupos de un sistema. \\}
\section*{Nuestro canal IRC}
Si desea comunicarse con nosotros, búsquenos en nuestro canal IRC de \emph{Freenode} \textbf{\#ramaieeeuns}.
\end{document}
